<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog\Test;

use PhpExtended\ApiFrInseeCog\ApiFrInseeCogCanton;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrInseeCogCantonTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeCog\ApiFrInseeCogCanton
 * @internal
 * @small
 */
class ApiFrInseeCogCantonTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeCogCanton
	 */
	protected ApiFrInseeCogCanton $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getId());
		$expected = 'qsdfghjklm';
		$this->_object->setId($expected);
		$this->assertEquals($expected, $this->_object->getId());
	}
	
	public function testGetFkArrondissement() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getFkArrondissement());
		$expected = 'qsdfghjklm';
		$this->_object->setFkArrondissement($expected);
		$this->assertEquals($expected, $this->_object->getFkArrondissement());
	}
	
	public function testGetFkTypeCanton() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getFkTypeCanton());
		$expected = 'qsdfghjklm';
		$this->_object->setFkTypeCanton($expected);
		$this->assertEquals($expected, $this->_object->getFkTypeCanton());
	}
	
	public function testGetFkCompositionCantonale() : void
	{
		$this->assertEquals(12, $this->_object->getFkCompositionCantonale());
		$expected = 25;
		$this->_object->setFkCompositionCantonale($expected);
		$this->assertEquals($expected, $this->_object->getFkCompositionCantonale());
	}
	
	public function testGetFkCommuneCheflieu() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getFkCommuneCheflieu());
		$expected = 'qsdfghjklm';
		$this->_object->setFkCommuneCheflieu($expected);
		$this->assertEquals($expected, $this->_object->getFkCommuneCheflieu());
	}
	
	public function testGetFkTncc() : void
	{
		$this->assertEquals(12, $this->_object->getFkTncc());
		$expected = 25;
		$this->_object->setFkTncc($expected);
		$this->assertEquals($expected, $this->_object->getFkTncc());
	}
	
	public function testGetNcc() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getNcc());
		$expected = 'qsdfghjklm';
		$this->_object->setNcc($expected);
		$this->assertEquals($expected, $this->_object->getNcc());
	}
	
	public function testGetNccenr() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getNccenr());
		$expected = 'qsdfghjklm';
		$this->_object->setNccenr($expected);
		$this->assertEquals($expected, $this->_object->getNccenr());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeCogCanton('azertyuiop', 'azertyuiop', 'azertyuiop', 12, 'azertyuiop', 12, 'azertyuiop', 'azertyuiop');
	}
	
}
