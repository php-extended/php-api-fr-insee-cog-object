<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog\Test;

use PhpExtended\ApiFrInseeCog\ApiFrInseeCogTypeCanton;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrInseeCogTypeCantonTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeCog\ApiFrInseeCogTypeCanton
 * @internal
 * @small
 */
class ApiFrInseeCogTypeCantonTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeCogTypeCanton
	 */
	protected ApiFrInseeCogTypeCanton $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getId());
		$expected = 'qsdfghjklm';
		$this->_object->setId($expected);
		$this->assertEquals($expected, $this->_object->getId());
	}
	
	public function testGetLibelle() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getLibelle());
		$expected = 'qsdfghjklm';
		$this->_object->setLibelle($expected);
		$this->assertEquals($expected, $this->_object->getLibelle());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeCogTypeCanton('azertyuiop', 'azertyuiop');
	}
	
}
