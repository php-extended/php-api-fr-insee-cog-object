<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpoint;
use PhpExtended\ApiFrInseeCog\ApiFrInseeCogPays;
use PhpExtended\ApiFrInseeCog\ApiFrInseeCogPaysHistory;
use PHPUnit\Framework\TestCase;

/**
 * InseeCogEndpointTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpoint
 *
 * @internal
 *
 * @small
 */
class ApiFrInseeCogEndpointTest extends TestCase
{
	
	protected static array $_idActualitePays = [];
	protected static array $_idTnccs = [];
	protected static array $_idCompCantonales = [];
	protected static array $_idTypeCantons = [];
	protected static array $_idTypeCommunes = [];
	protected static array $_idTypeEvents = [];
	protected static array $_idPays = [];
	protected static array $_idRegions = [];
	protected static array $_idDepartements = [];
	protected static array $_idArrondissements = [];
	protected static array $_idCantons = [];
	protected static array $_idCommunes = [];	
	
	/**
	 * The endpoint to test.
	 * 
	 * @var ApiFrInseeCogEndpoint
	 */
	protected ApiFrInseeCogEndpoint $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testMinYear() : void
	{
		$found = \scandir(\dirname(__DIR__).'/data');
		$minYear = \PHP_INT_MAX;
		
		foreach($found as $basename)
		{
			if('.' === $basename || '..' === $basename)
			{
			continue;
			}
			$path = \dirname(__DIR__).'/data/'.$basename;
			if(\is_dir($path))
			{
				if(\preg_match('#^\\d+$#', $basename))
				{
					$minYear = \min($minYear, (int) $basename);
				}
			}
		}
		
		$this->assertEquals($minYear, $this->_object->getMinimumAvailableYear());
	}
	
	public function testMaxYear() : void
	{
		$found = \scandir(\dirname(__DIR__).'/data');
		$maxYear = 0;
		
		foreach($found as $basename)
		{
			if('.' === $basename || '..' === $basename)
			{
			continue;
			}
			$path = \dirname(__DIR__).'/data/'.$basename;
			if(\is_dir($path))
			{
				if(\preg_match('#^\\d+$#', $basename))
				{
					$maxYear = \max($maxYear, (int) $basename);
				}
			}
		}
		
		$this->assertEquals($maxYear, $this->_object->getMaximumAvailableYear());
	}
	
	public function testMissedPaysIterator() : void
	{
		$this->assertInstanceOf(Iterator::class, $this->_object->getPaysIterator());
	}
	
	public function testPaysIterator() : void
	{
		$known = [];

		/** @var ApiFrInseeCogPays $pays */
		foreach($this->_object->getPaysIterator() as $pays)
		{
			$this->assertNotEmpty($pays->getId(), \strtr('Pays "{pays}" id must not be empty', ['{pays}' => $pays->getId()]));
			// assert unicity
			$this->assertNotContains($pays->getId(), $known);
			$known[] = $pays->getId();
			
			if(empty($pays->getFkPaysParent()))
			{
				$this->assertNull($pays->getFkPaysParent(), \strtr('Pays "{pays}" fk parent must be null', ['{pays}' => $pays->getId()]));
			}
			else
			{
				$this->assertNotEmpty($pays->getFkPaysParent(), \strtr('Pays "{pays}" fk parent must not be empty', ['{pays}' => $pays->getId()]));
				// some fails
				// $this->assertIsset($pays->getFkPaysParent(), self::$_idActualitePays \strtr('Pays "{pays}" fk parent must be present in pays id list', ['{pays}' => $pays->getId()]));
			}
			
			$this->assertNotEmpty($pays->getFkActualitePays(), \strtr('Pays "{pays}" fk actualite must not be empty', ['{pays}' => $pays->getId()]));
			$this->assertIsset($pays->getFkActualitePays(), self::$_idActualitePays, \strtr('Pays "{pays}" fk actualite must be present in actu pays id list', ['{pays}' => $pays->getId()]));
		
			
			$this->assertNotEmpty($pays->getLibCog(), \strtr('Pays "{pays}" libelle cog must not be empty', ['{pays}' => $pays->getId()]));
			$this->assertNotEmpty($pays->getLibEnr(), \strtr('Pays "{pays}" libelle enrichi must not be empty', ['{pays}' => $pays->getId()]));
			
			if(empty($pays->getIso2()))
			{
				$this->assertNull($pays->getIso2(), \strtr('Pays "{pays}" iso2 must be null', ['{pays}' => $pays->getId()]));
			}
			else
			{
				// TODO test unicity
				$this->assertNotEmpty($pays->getIso2(), \strtr('Pays "{pays}" iso2 must not be empty', ['{pays}' => $pays->getId()]));
			}
			
			if(empty($pays->getIso3()))
			{
				$this->assertNull($pays->getIso3(), \strtr('Pays "{pays}" iso3 must be null', ['{pays}' => $pays->getId()]));
			}
			else
			{
				// TODO test unicity
				$this->assertNotEmpty($pays->getIso3(), \strtr('Pays "{pays}" iso3 must not be empty', ['{pays}' => $pays->getId()]));
			}
			
			if(empty($pays->getIsonum3()))
			{
				$this->assertNull($pays->getIsonum3(), \strtr('Pays "{pays}" isonum3 must be null', ['{pays}' => $pays->getId()]));
			}
			else
			{
				// TODO test unicity
				$this->assertNotEmpty($pays->getIsonum3(), \strtr('Pays "{pays}" isonum3 must not be empty', ['{pays}' => $pays->getId()]));
			}
		}
	}

	public function testPaysHistoryIterator() : void
	{
		foreach($this->_object->getPaysHistoryIterator() as $paysHistory)
		{
			$this->assertInstanceOf(ApiFrInseeCogPaysHistory::class, $paysHistory);

			$this->assertNotEmpty($paysHistory->getFkPaysId());
			$this->assertNotEmpty($paysHistory->getLibCog());
			$this->assertNotEmpty($paysHistory->getLibEnr());
			$this->assertNotEmpty($paysHistory->getDateStart());
		}
	}
	
	public function testMissedRegionIterator() : void
	{
		$this->assertInstanceOf(Iterator::class, $this->_object->getRegionIterator(0));
	}
	
	public function testRegions() : void
	{
		for($year = $this->_object->getMinimumAvailableYear(); $this->_object->getMaximumAvailableYear() >= $year; $year++)
		{
			/** @var PhpExtended\ApiFrInseeCog\ApiFrInseeCogRegion $region */
			foreach($this->_object->getRegionIterator($year) as $region)
			{
				$this->assertNotEmpty($region->getId(), \strtr('Region {year} {region} id must not be empty', ['{year}' => $year, '{region}' => $region->getId()]));
				$this->assertNotEmpty($region->getFkPays(), \strtr('Region {year} {region} fk pays must not be empty', ['{year}' => $year, '{region}' => $region->getId()]));
				$this->assertIsset($region->getFkPays(), ApiFrInseeCogEndpointTest::$_idPays, \strtr('Region {year} {region} fk pays must exist in pays list', ['{year}' => $year, '{region}' => $region->getId()]));
				$this->assertNotEmpty($region->getFkCommuneCheflieu(), \strtr('Region {year} {region} cheflieu must not be empty', ['{year}' => $year, '{region}' => $region->getId()]));
				$this->assertIsset($region->getFkCommuneCheflieu(), ApiFrInseeCogEndpointTest::$_idCommunes[$year], \strtr('Region {year} {region} cheflieu must exist in cheflieu list', ['{year}' => $year, '{region}' => $region->getId()]));
				// do not check tncc, may be zero
				$this->assertIsset($region->getFkTncc(), ApiFrInseeCogEndpointTest::$_idTnccs, \strtr('Region {year} {region} tncc must exist in tncc list', ['{year}' => $year, '{region}' => $region->getId()]));
				$this->assertNotEmpty($region->getNcc(), \strtr('Region {year} {region} ncc must not be empty', ['{year}' => $year, '{region}' => $region->getId()]));
				$this->assertNotEmpty($region->getNccenr(), \strtr('Region {year} {region} nccenr must not be empty', ['{year}' => $year, '{region}' => $region->getId()]));
			}
		}
	}
	
	public function testMissedDepartementIterator() : void
	{
		$this->assertInstanceOf(Iterator::class, $this->_object->getDepartementIterator(0));
	}
	
	public function testDepartements() : void
	{
		for($year = $this->_object->getMinimumAvailableYear(); $this->_object->getMaximumAvailableYear() >= $year; $year++)
		{
			/** @var PhpExtended\ApiFrInseeCog\ApiFrInseeCogDepartement $departement */
			foreach($this->_object->getDepartementIterator($year) as $departement)
			{
				$this->assertNotEmpty($departement->getId(), \strtr('Departement {year} {dept} id must not be empty', ['{year}' => $year, '{dept}' => $departement->getId()]));
				$this->assertNotEmpty($departement->getFkRegion(), \strtr('Departement {year} {dept} fk region must not be empty', ['{year}' => $year, '{dept}' => $departement->getId()]));
				$this->assertIsset($departement->getFkRegion(), ApiFrInseeCogEndpointTest::$_idRegions[$year], \strtr('Departement {year} {dept} fk region must exist in region list', ['{year}' => $year, '{dept}' => $departement->getId()]));
				$this->assertNotEmpty($departement->getFkCommuneCheflieu(), \strtr('Departement {year} {dept} cheflieu must not be empty', ['{year}' => $year, '{dept}' => $departement->getId()]));
				$this->assertIsset($departement->getFkCommuneCheflieu(), ApiFrInseeCogEndpointTest::$_idCommunes[$year], \strtr('Departement {year} {dept} cheflieu must exist in cheflieu list', ['{year}' => $year, '{dept}' => $departement->getId()]));
				// do not check tncc, may be zero
				$this->assertIsset($departement->getFkTncc(), ApiFrInseeCogEndpointTest::$_idTnccs, \strtr('Departement {year} {dept} tncc must exist in tncc list', ['{year}' => $year, '{dept}' => $departement->getId()]));
				$this->assertNotEmpty($departement->getNcc(), \strtr('Departement {year} {dept} ncc must not be empty', ['{year}' => $year, '{dept}' => $departement->getId()]));
				$this->assertNotEmpty($departement->getNccenr(), \strtr('Departement {year} {dept} nccenr must not be empty', ['{year}' => $year, '{dept}' => $departement->getId()]));
			}
		}
	}
	
	public function testMissedArrondissementIterator() : void
	{
		$this->assertInstanceOf(Iterator::class, $this->_object->getArrondissementIterator(0));
	}
	
	public function testArrondissements() : void
	{
		for($year = $this->_object->getMinimumAvailableYear(); $this->_object->getMaximumAvailableYear() >= $year; $year++)
		{
			/** @var PhpExtended\ApiFrInseeCog\ApiFrInseeCogArrondissement $arrondissement */
			foreach($this->_object->getArrondissementIterator($year) as $arrondissement)
			{
				$this->assertNotEmpty($arrondissement->getId(), \strtr('Arrondissement {year} {arrd} id must not be empty', ['{year}' => $year, '{arrd}' => $arrondissement->getId()]));
				$this->assertNotEmpty($arrondissement->getFkDepartement(), \strtr('Arrondissement {year} {arrd} fk departement must not be empty', ['{year}' => $year, '{arrd}' => $arrondissement->getId()]));
				$this->assertIsset($arrondissement->getFkDepartement(), ApiFrInseeCogEndpointTest::$_idDepartements[$year], \strtr('Arrondissement {year} {arrd} fk departement must exist in departement list', ['{year}' => $year, '{arrd}' => $arrondissement->getId()]));
				$this->assertNotEmpty($arrondissement->getFkCommuneCheflieu(), \strtr('Arrondissement {year} {arrd} cheflieu must not be empty', ['{year}' => $year, '{arrd}' => $arrondissement->getId()]));
				$this->assertIsset($arrondissement->getFkCommuneCheflieu(), ApiFrInseeCogEndpointTest::$_idCommunes[$year], \strtr('Arrondissement {year} {arrd} cheflieu must exist in cheflieu list', ['{year}' => $year, '{arrd}' => $arrondissement->getId()]));
				// do not check tncc, may be zero
				$this->assertIsset($arrondissement->getFkTncc(), ApiFrInseeCogEndpointTest::$_idTnccs, \strtr('Arrondissement {year} {arrd} tncc must exist in tncc list', ['{year}' => $year, '{arrd}' => $arrondissement->getId()]));
				$this->assertNotEmpty($arrondissement->getNcc(), \strtr('Arrondissement {year} {arrd} ncc must not be empty', ['{year}' => $year, '{arrd}' => $arrondissement->getId()]));
				$this->assertNotEmpty($arrondissement->getNccenr(), \strtr('Arrondissement {year} {arrd} nccenr must not be empty', ['{year}' => $year, '{arrd}' => $arrondissement->getId()]));
			}
		}
	}
	
	public function testMissedCantonIterator() : void
	{
		$this->assertInstanceOf(Iterator::class, $this->_object->getCantonIterator(0));
	}
	
	public function testCantons() : void
	{
		$allCacheFilePath = \dirname(__DIR__).'/all.cache';
		$minYear = \is_file($allCacheFilePath) 
			? $this->_object->getMinimumAvailableYear()
			: $this->_object->getMaximumAvailableYear();
		
		for($year = $minYear; $this->_object->getMaximumAvailableYear() >= $year; $year++)
		{
			/** @var PhpExtended\ApiFrInseeCog\ApiFrInseeCogCanton $canton */
			foreach($this->_object->getCantonIterator($year) as $canton)
			{
				$this->assertNotEmpty($canton->getId(), \strtr('Canton {year} {canton} id must not be empty', ['{year}' => $year, '{canton}' => $canton->getId()]));
				$this->assertNotEmpty($canton->getFkArrondissement(), \strtr('Canton {year} {canton} fk arrondissement must not be empty', ['{year}' => $year, '{canton}' => $canton->getId()]));
				$this->assertIsset($canton->getFkArrondissement(), ApiFrInseeCogEndpointTest::$_idArrondissements[$year], \strtr('Canton {year} {canton} fk arrondissement must exist in arrondissement list', ['{year}' => $year, '{canton}' => $canton->getId()]));
				$this->assertNotEmpty($canton->getFkTypeCanton(), \strtr('Canton {year} {canton} type canton must not be empty', ['{year}' => $year, '{canton}' => $canton->getId()]));
				$this->assertIsset($canton->getFkTypeCanton(), ApiFrInseeCogEndpointTest::$_idTypeCantons, \strtr('Canton {year} {canton} type canton must exist in type canton list', ['{year}' => $year, '{canton}' => $canton->getId()]));
				$this->assertNotEmpty($canton->getFkCompositionCantonale(), \strtr('Canton {year} {canton} composition must not be empty', ['{year}' => $year, '{canton}' => $canton->getId()]));
				$this->assertIsset($canton->getFkCompositionCantonale(), ApiFrInseeCogEndpointTest::$_idCompCantonales, \strtr('Canton {year} {canton} composition must exist in composition list', ['{year}' => $year, '{canton}' => $canton->getId()]));
				$this->assertNotEmpty($canton->getFkCommuneCheflieu(), \strtr('Canton {year} {canton} cheflieu must not be empty', ['{year}' => $year, '{canton}' => $canton->getId()]));
				$this->assertIsset($canton->getFkCommuneCheflieu(), ApiFrInseeCogEndpointTest::$_idCommunes[$year], \strtr('Canton {year} {canton} cheflieu must exist in cheflieu list', ['{year}' => $year, '{canton}' => $canton->getId()]));
				// do not check tncc, may be zero
				$this->assertIsset($canton->getFkTncc(), ApiFrInseeCogEndpointTest::$_idTnccs, \strtr('Canton {year} {canton} tncc must exist in tncc list', ['{year}' => $year, '{canton}' => $canton->getId()]));
				$this->assertNotEmpty($canton->getNcc(), \strtr('Canton {year} {canton} ncc must not be empty', ['{year}' => $year, '{canton}' => $canton->getId()]));
				$this->assertNotEmpty($canton->getNccenr(), \strtr('Canton {year} {canton} nccenr must not be empty', ['{year}' => $year, '{canton}' => $canton->getId()]));
			}
		}
	}

	public function testCollectivitesTerritoriales() : void
	{
		$allCacheFilePath = \dirname(__DIR__).'/all.cache';
		$minYear = \is_file($allCacheFilePath)
			? $this->_object->getMinimumAvailableYear()
			: $this->_object->getMaximumAvailableYear();
		
		for($year = $minYear; $this->_object->getMaximumAvailableYear() >= $year; $year++)
		{
			/** @var PhpExtended\ApiFrInseeCog\ApiFrInseeCogCollectiviteTerritoriale $ctcd */
			foreach($this->_object->getCollectiviteTerritorialeIterator($year) as $ctcd)
			{
				$this->assertNotEmpty($ctcd->getId());
				$this->assertNotEmpty($ctcd->getFkRegion());
				$this->assertIsset($ctcd->getFkRegion(), ApiFrInseeCogEndpointTest::$_idRegions[$year]);
				$this->assertNotEmpty($ctcd->getFkCommuneCheflieu());
				$this->assertIsset($ctcd->getFkCommuneCheflieu(), ApiFrInseeCogEndpointTest::$_idCommunes[$year]);
				$this->assertNotEmpty($ctcd->getFkTncc());
				$this->assertIsset($ctcd->getFkTncc(), ApiFrInseeCogEndpointTest::$_idTnccs);
				$this->assertNotEmpty($ctcd->getNcc());
				$this->assertNotEmpty($ctcd->getNccenr());
				$this->assertNotEmpty($ctcd->getLibelle());
			}
		}
	}
	
	public function testMissedCommuneIterator() : void
	{
		$this->assertInstanceOf(Iterator::class, $this->_object->getCommuneIterator(0));
	}
	
	public function testCommunes() : void
	{
		$allCacheFilePath = \dirname(__DIR__).'/all.cache';
		$minYear = \is_file($allCacheFilePath)
			? $this->_object->getMinimumAvailableYear()
			: $this->_object->getMaximumAvailableYear();
		
		for($year = $minYear; $this->_object->getMaximumAvailableYear() >= $year; $year++)
		{
// 			echo $year."\n";
			
// 			foreach($this->_endpoint->getCommuneIterator($year) as $commune)
// 			{
// 				if(!isset(InseeCogEndpointTest::$_idCantons[$year][$commune->getFkCanton()]))
// 				{
// 					echo "if('".$commune->getFkCanton()."' === \$newline[2]) \$newline[2] = ''; // ".$commune->getNcc()."\n";
// 				}
// 			}
			
			/** @var PhpExtended\ApiFrInseeCog\ApiFrInseeCogCommune $commune */
			foreach($this->_object->getCommuneIterator($year) as $commune)
			{
				$this->assertNotEmpty($commune->getId(), \strtr('Commune {year} {commune} id must not be empty', ['{year}' => $year, '{commune}' => $commune->getId()]));
				$this->assertNotEmpty($commune->getFkTypeCommune(), \strtr('Commune {year} {commune} fk type commune must not be empty', ['{year}' => $year, '{commune}' => $commune->getId()]));
				$this->assertIsset($commune->getFkTypeCommune(), ApiFrInseeCogEndpointTest::$_idTypeCommunes, \strtr('Commune {year} {commune} fk type commune must exist in type commune list', ['{year}' => $year, '{commune}' => $commune->getId()]));
				$this->assertNotEmpty($commune->getFkDepartement(), \strtr('Commune {year} {commune} fk departement must not be empty', ['{year}' => $year, '{commune}' => $commune->getId()]));
				$this->assertIsset($commune->getFkDepartement(), ApiFrInseeCogEndpointTest::$_idDepartements[$year], \strtr('Commune {year} {commune} fk departement must exist in departement list', ['{year}' => $year, '{commune}' => $commune->getId()]));
				
				if(empty($commune->getFkCommuneParent()))
				{
					$this->assertNull($commune->getFkCommuneParent(), \strtr('Commune {year} {commune} commune parent must be null', ['{year}' => $year, '{commune}' => $commune->getId()]));
				}
				else
				{
					$this->assertIsset($commune->getFkCommuneParent(), ApiFrInseeCogEndpointTest::$_idCommunes[$year], \strtr('Commune {year} {commune} commune parent must exist in commune parent list', ['{year}' => $year, '{commune}' => $commune->getId()]));
				}
				
				// do not check tncc, may be zero
				$this->assertIsset($commune->getFkTncc(), ApiFrInseeCogEndpointTest::$_idTnccs, \strtr('Commune {year} {commune} tncc must exist in tncc list', ['{year}' => $year, '{commune}' => $commune->getId()]));
				$this->assertNotEmpty($commune->getNcc(), \strtr('Commune {year} {commune} ncc must not be empty', ['{year}' => $year, '{commune}' => $commune->getId()]));
				$this->assertNotEmpty($commune->getNccenr(), \strtr('Commune {year} {commune} nccenr must not be empty', ['{year}' => $year, '{commune}' => $commune->getId()]));
			}
		}
	}
	
	public function testEvents() : void
	{
		/** @var PhpExtended\ApiFrInseeCog\ApiFrInseeCogEventCommune $event */
		foreach($this->_object->getEventCommuneIterator() as $event)
		{
			$this->assertNotEmpty($event->getDateEffet(), \strtr('Event {event} date effet must not be empty', ['{event}' => $event->getDateEffet()->format('Y-m-d')]));
			$this->assertNotEmpty($event->getFkTypeEventCommune(), \strtr('Event {event} type event must not be empty', ['{event}' => $event->getDateEffet()->format('Y-m-d')]));
			$this->assertIsset($event->getFkTypeEventCommune(), ApiFrInseeCogEndpointTest::$_idTypeEvents, \strtr('Event {event} type event must exist in type event list', ['{event}' => $event->getDateEffet()->format('Y-m-d')]));
			$this->assertNotEmpty($event->getFkTypeCommuneBefore(), \strtr('Event {event} type commune before must not be empty', ['{event}' => $event->getDateEffet()->format('Y-m-d')]));
			$this->assertIsset($event->getFkTypeCommuneBefore(), ApiFrInseeCogEndpointTest::$_idTypeCommunes, \strtr('Event {event} type commune before must exist in type commune list', ['{event}' => $event->getDateEffet()->format('Y-m-d')]));
// 			$year = (int) $event->getDateEffet()->format('Y');
			
			// TODO check ?
// 			if(isset(InseeCogEndpointTest::$_idCommunes[$year + 1]) && (2018 <= $year || 'COM' === $event->getFkTypeCommuneBefore()))
// 			{
// 				$this->assertIsset($event->getFkCommuneBefore(), InseeCogEndpointTest::$_idCommunes[$year + 1], \strtr('Event {event} id commune before must exist in commune list', ['{event}' => $event->getDateEffet()->format('Y-m-d')]));
// 			}
			
			$this->assertNotEmpty($event->getFkTypeCommuneBefore(), \strtr('Event {event} type commune before must not be empty', ['{event}' => $event->getDateEffet()->format('Y-m-d')]));
			$this->assertIsset($event->getFkTypeCommuneAfter(), ApiFrInseeCogEndpointTest::$_idTypeCommunes, \strtr('Event {event} type commune after must exist in type commune list', ['{event}' => $event->getDateEffet()->format('Y-m-d')]));
			
			// TODO check ?
// 			if(isset(InseeCogEndpointTest::$_idCommunes[$year + 1]) && (2018 <= $year || 'COM' === $event->getFkTypeCommuneAfter()))
// 			{
// 				$this->assertIsset($event->getFkCommuneAfter(), InseeCogEndpointTest::$_idCommunes[$year + 1], \strtr('Event {event} id commune after must exist in commune list', ['{event}' => $event->getDateEffet()->format('Y-m-d')]));
// 			}
			
			// do not check tncc, may be zero
			$this->assertIsset($event->getFkTnccBefore(), ApiFrInseeCogEndpointTest::$_idTnccs, \strtr('Event {event} tncc before must exist in tncc list', ['{event}' => $event->getDateEffet()->format('Y-m-d')]));
			$this->assertNotEmpty($event->getNccBefore(), \strtr('Event {event} ncc before must not be empty', ['{event}' => $event->getDateEffet()->format('Y-m-d')]));
			$this->assertNotEmpty($event->getNccenrBefore(), \strtr('Event {event} nccenr after must not be empty', ['{event}' => $event->getDateEffet()->format('Y-m-d')]));
			
			// do not check tncc, may be zero
			$this->assertIsset($event->getFkTnccAfter(), ApiFrInseeCogEndpointTest::$_idTnccs, \strtr('Event {event} tncc after must exist in tncc list', ['{event}' => $event->getDateEffet()->format('Y-m-d')]));
			$this->assertNotEmpty($event->getNccAfter(), \strtr('Event {event} ncc after must not be empty', ['{event}' => $event->getDateEffet()->format('Y-m-d')]));
			$this->assertNotEmpty($event->getNccenrAfter(), \strtr('Event {event} nccenr after must not be empty', ['{event}' => $event->getDateEffet()->format('Y-m-d')]));
		}
	}
	
	public function testActualitePays() : void
	{
		/** @var PhpExtended\ApiFrInseeCog\ApiFrInseeCogActualitePays $actu */
		foreach($this->_object->getActualitePaysIterator() as $actu)
		{
			$this->assertNotEmpty($actu->getId());
			$this->assertNotEmpty($actu->getLibelle());
		}
	}
	
	public function testActualitePaysRecord() : void
	{
		$this->assertNotNull($this->_object->getActualitePays(1));
	}
	
	public function testCompositionCantonale() : void
	{
		/** @var PhpExtended\ApiFrInseeCog\ApiFrInseeCogCompositionCantonale $comp */
		foreach($this->_object->getCompositionCantonaleIterator() as $comp)
		{
			$this->assertNotEmpty($comp->getId());
			$this->assertNotEmpty($comp->getLibelle());
		}
	}
	
	public function testCompositionCantonaleRecord() : void
	{
		$this->assertNotNull($this->_object->getCompositionCantonale(1));
	}
	
	public function testTncc() : void
	{
		/** @var PhpExtended\ApiFrInseeCog\ApiFrInseeCogTncc $tncc */
		foreach($this->_object->getTnccIterator() as $tncc)
		{
			// do not check id, may be zero
			// do not check article, may be empty
			$this->assertNotEmpty($tncc->getCharniere());
			// do not check space, may be empty
		}
	}
	
	public function testTnccRecord() : void
	{
		$this->assertNotNull($this->_object->getTncc(1));
	}
	
	public function testTypeCanton() : void
	{
		/** @var PhpExtended\ApiFrInseeCog\ApiFrInseeCogTypeCanton $typeCanton */
		foreach($this->_object->getTypeCantonIterator() as $typeCanton)
		{
			$this->assertNotEmpty($typeCanton->getId());
			$this->assertNotEmpty($typeCanton->getLibelle());
		}
	}
	
	public function testTypeCantonRecord() : void
	{
		$this->assertNotNull($this->_object->getTypeCanton('C'));
	}
	
	public function testTypeCommune() : void
	{
		/** @var PhpExtended\ApiFrInseeCog\ApiFrInseeCogTypeCommune $typeCommune */
		foreach($this->_object->getTypeCommuneIterator() as $typeCommune)
		{
			$this->assertNotEmpty($typeCommune->getId());
			$this->assertNotEmpty($typeCommune->getLibelle());
		}
	}
	
	public function testTypeCommuneRecord() : void
	{
		$this->assertNotNull($this->_object->getTypeCommune('COM'));
	}
	
	public function testTypeEvent() : void
	{
		/** @var PhpExtended\ApiFrInseeCog\ApiFrInseeCogTypeEventCommune $typeEvent */
		foreach($this->_object->getTypeEventCommuneIterator() as $typeEvent)
		{
			$this->assertNotEmpty($typeEvent->getId());
			$this->assertNotEmpty($typeEvent->getLibelle());
		}
	}
	
	public function testTypeEventCommuneRecord() : void
	{
		$this->assertNotNull($this->_object->getTypeEventCommune(10));
	}
	
	/**
	 * Extended function to assert whether the needle is in the haystack.
	 * 
	 * @param integer|string $needle
	 * @param array $haystack
	 * @param string $message
	 */
	protected function assertIsset($needle, array $haystack, string $message = '') : void
	{
		$this->assertTrue(
			isset($haystack[$needle]),
			$message
			."\n".'Requested value : "'.$needle.'"',
			// ."\n".'Available values : "'.\implode('", "', \array_keys($haystack)).'"'
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUpBeforeClass()
	 */
	public static function setUpBeforeClass() : void
	{
		$endpoint = new ApiFrInseeCogEndpoint();
		
		/** @var PhpExtended\ApiFrInseeCog\ApiFrInseeCogActualitePays $actu */
		foreach($endpoint->getActualitePaysIterator() as $actu)
		{
			self::$_idActualitePays[$actu->getId()] = 1;
		}
		
		/** @var PhpExtended\ApiFrInseeCog\ApiFrInseeCogTncc $tncc */
		foreach($endpoint->getTnccIterator() as $tncc)
		{
			self::$_idTnccs[$tncc->getId()] = 1;
		}
		
		for($year = $endpoint->getMinimumAvailableYear(); $endpoint->getMaximumAvailableYear() >= $year; $year++)
		{
			/** @var PhpExtended\ApiFrInseeCog\ApiFrInseeCogCommune $commune */
			foreach($endpoint->getCommuneIterator($year) as $commune)
			{
				self::$_idCommunes[$year][$commune->getId()] = 1;
			}
			
			/** @var PhpExtended\ApiFrInseeCog\ApiFrInseeCogCanton $canton */
			foreach($endpoint->getCantonIterator($year) as $canton)
			{
				self::$_idCantons[$year][$canton->getId()] = 1;
			}
			
			/** @var PhpExtended\ApiFrInseeCog\ApiFrInseeCogArrondissement $arrondissement */
			foreach($endpoint->getArrondissementIterator($year) as $arrondissement)
			{
				self::$_idArrondissements[$year][$arrondissement->getId()] = 1;
			}
			
			/** @var PhpExtended\ApiFrInseeCog\ApiFrInseeCogDepartement $departement */
			foreach($endpoint->getDepartementIterator($year) as $departement)
			{
				self::$_idDepartements[$year][$departement->getId()] = 1;
			}
			
			/** @var PhpExtended\ApiFrInseeCog\ApiFrInseeCogRegion $region */
			foreach($endpoint->getRegionIterator($year) as $region)
			{
				self::$_idRegions[$year][$region->getId()] = 1;
			}
		}

		/** @var ApiFrInseeCogPays $pays */
		foreach($endpoint->getPaysIterator() as $pays)
		{
			self::$_idPays[$pays->getId()] = 1;
		}
		
		/** @var PhpExtended\ApiFrInseeCog\ApiFrInseeCogTypeCommune $typeCommune */
		foreach($endpoint->getTypeCommuneIterator() as $typeCommune)
		{
			self::$_idTypeCommunes[$typeCommune->getId()] = 1;
		}
		
		/** @var PhpExtended\ApiFrInseeCog\ApiFrInseeCogTypeEventCommune $typeEvent */
		foreach($endpoint->getTypeEventCommuneIterator() as $typeEvent)
		{
			self::$_idTypeEvents[$typeEvent->getId()] = 1;
		}
		
		/** @var PhpExtended\ApiFrInseeCog\ApiFrInseeCogTypeCanton $typeCanton */
		foreach($endpoint->getTypeCantonIterator() as $typeCanton)
		{
			self::$_idTypeCantons[$typeCanton->getId()] = 1;
		}
		
		/** @var PhpExtended\ApiFrInseeCog\ApiFrInseeCogCompositionCantonale $composition */
		foreach($endpoint->getCompositionCantonaleIterator() as $composition)
		{
			self::$_idCompCantonales[$composition->getId()] = 1;
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeCogEndpoint();
	}
	
}
