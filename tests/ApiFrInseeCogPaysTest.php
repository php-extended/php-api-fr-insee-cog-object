<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog\Test;

use PhpExtended\ApiFrInseeCog\ApiFrInseeCogPays;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrInseeCogPaysTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeCog\ApiFrInseeCogPays
 * @internal
 * @small
 */
class ApiFrInseeCogPaysTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeCogPays
	 */
	protected ApiFrInseeCogPays $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals(12, $this->_object->getId());
		$expected = 25;
		$this->_object->setId($expected);
		$this->assertEquals($expected, $this->_object->getId());
	}
	
	public function testGetFkActualitePays() : void
	{
		$this->assertEquals(12, $this->_object->getFkActualitePays());
		$expected = 25;
		$this->_object->setFkActualitePays($expected);
		$this->assertEquals($expected, $this->_object->getFkActualitePays());
	}
	
	public function testGetFkPaysParent() : void
	{
		$this->assertNull($this->_object->getFkPaysParent());
		$expected = 25;
		$this->_object->setFkPaysParent($expected);
		$this->assertEquals($expected, $this->_object->getFkPaysParent());
	}
	
	public function testGetCreationYear() : void
	{
		$this->assertNull($this->_object->getCreationYear());
		$expected = 25;
		$this->_object->setCreationYear($expected);
		$this->assertEquals($expected, $this->_object->getCreationYear());
	}
	
	public function testGetLibCog() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getLibCog());
		$expected = 'qsdfghjklm';
		$this->_object->setLibCog($expected);
		$this->assertEquals($expected, $this->_object->getLibCog());
	}
	
	public function testGetLibEnr() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getLibEnr());
		$expected = 'qsdfghjklm';
		$this->_object->setLibEnr($expected);
		$this->assertEquals($expected, $this->_object->getLibEnr());
	}
	
	public function testGetIso2() : void
	{
		$this->assertNull($this->_object->getIso2());
		$expected = 'qsdfghjklm';
		$this->_object->setIso2($expected);
		$this->assertEquals($expected, $this->_object->getIso2());
	}
	
	public function testGetIso3() : void
	{
		$this->assertNull($this->_object->getIso3());
		$expected = 'qsdfghjklm';
		$this->_object->setIso3($expected);
		$this->assertEquals($expected, $this->_object->getIso3());
	}
	
	public function testGetIsonum3() : void
	{
		$this->assertNull($this->_object->getIsonum3());
		$expected = 'qsdfghjklm';
		$this->_object->setIsonum3($expected);
		$this->assertEquals($expected, $this->_object->getIsonum3());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeCogPays(12, 12, 'azertyuiop', 'azertyuiop');
	}
	
}
