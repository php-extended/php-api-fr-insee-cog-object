<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog\Test;

use DateTimeImmutable;
use PhpExtended\ApiFrInseeCog\ApiFrInseeCogEventCommune;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrInseeCogEventCommuneTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeCog\ApiFrInseeCogEventCommune
 * @internal
 * @small
 */
class ApiFrInseeCogEventCommuneTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeCogEventCommune
	 */
	protected ApiFrInseeCogEventCommune $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetDateEffet() : void
	{
		$this->assertEquals(DateTimeImmutable::createFromFormat('!Y-m-d', '2000-01-01'), $this->_object->getDateEffet());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01');
		$this->_object->setDateEffet($expected);
		$this->assertEquals($expected, $this->_object->getDateEffet());
	}
	
	public function testGetFkTypeEventCommune() : void
	{
		$this->assertEquals(12, $this->_object->getFkTypeEventCommune());
		$expected = 25;
		$this->_object->setFkTypeEventCommune($expected);
		$this->assertEquals($expected, $this->_object->getFkTypeEventCommune());
	}
	
	public function testGetFkTypeCommuneBefore() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getFkTypeCommuneBefore());
		$expected = 'qsdfghjklm';
		$this->_object->setFkTypeCommuneBefore($expected);
		$this->assertEquals($expected, $this->_object->getFkTypeCommuneBefore());
	}
	
	public function testGetFkCommuneBefore() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getFkCommuneBefore());
		$expected = 'qsdfghjklm';
		$this->_object->setFkCommuneBefore($expected);
		$this->assertEquals($expected, $this->_object->getFkCommuneBefore());
	}
	
	public function testGetFkTypeCommuneAfter() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getFkTypeCommuneAfter());
		$expected = 'qsdfghjklm';
		$this->_object->setFkTypeCommuneAfter($expected);
		$this->assertEquals($expected, $this->_object->getFkTypeCommuneAfter());
	}
	
	public function testGetFkCommuneAfter() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getFkCommuneAfter());
		$expected = 'qsdfghjklm';
		$this->_object->setFkCommuneAfter($expected);
		$this->assertEquals($expected, $this->_object->getFkCommuneAfter());
	}
	
	public function testGetFkTnccBefore() : void
	{
		$this->assertEquals(12, $this->_object->getFkTnccBefore());
		$expected = 25;
		$this->_object->setFkTnccBefore($expected);
		$this->assertEquals($expected, $this->_object->getFkTnccBefore());
	}
	
	public function testGetNccBefore() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getNccBefore());
		$expected = 'qsdfghjklm';
		$this->_object->setNccBefore($expected);
		$this->assertEquals($expected, $this->_object->getNccBefore());
	}
	
	public function testGetNccenrBefore() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getNccenrBefore());
		$expected = 'qsdfghjklm';
		$this->_object->setNccenrBefore($expected);
		$this->assertEquals($expected, $this->_object->getNccenrBefore());
	}
	
	public function testGetFkTnccAfter() : void
	{
		$this->assertEquals(12, $this->_object->getFkTnccAfter());
		$expected = 25;
		$this->_object->setFkTnccAfter($expected);
		$this->assertEquals($expected, $this->_object->getFkTnccAfter());
	}
	
	public function testGetNccAfter() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getNccAfter());
		$expected = 'qsdfghjklm';
		$this->_object->setNccAfter($expected);
		$this->assertEquals($expected, $this->_object->getNccAfter());
	}
	
	public function testGetNccenrAfter() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getNccenrAfter());
		$expected = 'qsdfghjklm';
		$this->_object->setNccenrAfter($expected);
		$this->assertEquals($expected, $this->_object->getNccenrAfter());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeCogEventCommune(DateTimeImmutable::createFromFormat('!Y-m-d', '2000-01-01'), 12, 'azertyuiop', 'azertyuiop', 'azertyuiop', 'azertyuiop', 12, 'azertyuiop', 'azertyuiop', 12, 'azertyuiop', 'azertyuiop');
	}
	
}
