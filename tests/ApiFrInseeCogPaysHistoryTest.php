<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog\Test;

use DateTimeImmutable;
use PhpExtended\ApiFrInseeCog\ApiFrInseeCogPaysHistory;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrInseeCogPaysHistoryTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrInseeCog\ApiFrInseeCogPaysHistory
 * @internal
 * @small
 */
class ApiFrInseeCogPaysHistoryTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrInseeCogPaysHistory
	 */
	protected ApiFrInseeCogPaysHistory $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetFkPaysId() : void
	{
		$this->assertEquals(12, $this->_object->getFkPaysId());
		$expected = 25;
		$this->_object->setFkPaysId($expected);
		$this->assertEquals($expected, $this->_object->getFkPaysId());
	}
	
	public function testGetFkPaysBeforeId() : void
	{
		$this->assertNull($this->_object->getFkPaysBeforeId());
		$expected = 25;
		$this->_object->setFkPaysBeforeId($expected);
		$this->assertEquals($expected, $this->_object->getFkPaysBeforeId());
	}
	
	public function testGetLibCog() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getLibCog());
		$expected = 'qsdfghjklm';
		$this->_object->setLibCog($expected);
		$this->assertEquals($expected, $this->_object->getLibCog());
	}
	
	public function testGetLibEnr() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getLibEnr());
		$expected = 'qsdfghjklm';
		$this->_object->setLibEnr($expected);
		$this->assertEquals($expected, $this->_object->getLibEnr());
	}
	
	public function testGetDateStart() : void
	{
		$this->assertEquals(DateTimeImmutable::createFromFormat('!Y-m-d', '2000-01-01'), $this->_object->getDateStart());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01');
		$this->_object->setDateStart($expected);
		$this->assertEquals($expected, $this->_object->getDateStart());
	}
	
	public function testGetDateEnd() : void
	{
		$this->assertNull($this->_object->getDateEnd());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01');
		$this->_object->setDateEnd($expected);
		$this->assertEquals($expected, $this->_object->getDateEnd());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrInseeCogPaysHistory(12, 'azertyuiop', 'azertyuiop', DateTimeImmutable::createFromFormat('!Y-m-d', '2000-01-01'));
	}
	
}
