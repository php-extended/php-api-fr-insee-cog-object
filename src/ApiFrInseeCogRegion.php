<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog;

/**
 * ApiFrInseeCogRegion class file.
 * 
 * This is a simple implementation of the ApiFrInseeCogRegionInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrInseeCogRegion implements ApiFrInseeCogRegionInterface
{
	
	/**
	 * The id of this region.
	 * 
	 * @var string
	 */
	protected string $_id;
	
	/**
	 * The fk of the related pays.
	 * 
	 * @var string
	 */
	protected string $_fkPays;
	
	/**
	 * The fk of the related commune as cheflieu.
	 * 
	 * @var string
	 */
	protected string $_fkCommuneCheflieu;
	
	/**
	 * The fk of the type of name of this region.
	 * 
	 * @var int
	 */
	protected int $_fkTncc;
	
	/**
	 * The name of this region.
	 * 
	 * @var string
	 */
	protected string $_ncc;
	
	/**
	 * The enriched name of this region.
	 * 
	 * @var string
	 */
	protected string $_nccenr;
	
	/**
	 * Constructor for ApiFrInseeCogRegion with private members.
	 * 
	 * @param string $id
	 * @param string $fkPays
	 * @param string $fkCommuneCheflieu
	 * @param int $fkTncc
	 * @param string $ncc
	 * @param string $nccenr
	 */
	public function __construct(string $id, string $fkPays, string $fkCommuneCheflieu, int $fkTncc, string $ncc, string $nccenr)
	{
		$this->setId($id);
		$this->setFkPays($fkPays);
		$this->setFkCommuneCheflieu($fkCommuneCheflieu);
		$this->setFkTncc($fkTncc);
		$this->setNcc($ncc);
		$this->setNccenr($nccenr);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of this region.
	 * 
	 * @param string $id
	 * @return ApiFrInseeCogRegionInterface
	 */
	public function setId(string $id) : ApiFrInseeCogRegionInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of this region.
	 * 
	 * @return string
	 */
	public function getId() : string
	{
		return $this->_id;
	}
	
	/**
	 * Sets the fk of the related pays.
	 * 
	 * @param string $fkPays
	 * @return ApiFrInseeCogRegionInterface
	 */
	public function setFkPays(string $fkPays) : ApiFrInseeCogRegionInterface
	{
		$this->_fkPays = $fkPays;
		
		return $this;
	}
	
	/**
	 * Gets the fk of the related pays.
	 * 
	 * @return string
	 */
	public function getFkPays() : string
	{
		return $this->_fkPays;
	}
	
	/**
	 * Sets the fk of the related commune as cheflieu.
	 * 
	 * @param string $fkCommuneCheflieu
	 * @return ApiFrInseeCogRegionInterface
	 */
	public function setFkCommuneCheflieu(string $fkCommuneCheflieu) : ApiFrInseeCogRegionInterface
	{
		$this->_fkCommuneCheflieu = $fkCommuneCheflieu;
		
		return $this;
	}
	
	/**
	 * Gets the fk of the related commune as cheflieu.
	 * 
	 * @return string
	 */
	public function getFkCommuneCheflieu() : string
	{
		return $this->_fkCommuneCheflieu;
	}
	
	/**
	 * Sets the fk of the type of name of this region.
	 * 
	 * @param int $fkTncc
	 * @return ApiFrInseeCogRegionInterface
	 */
	public function setFkTncc(int $fkTncc) : ApiFrInseeCogRegionInterface
	{
		$this->_fkTncc = $fkTncc;
		
		return $this;
	}
	
	/**
	 * Gets the fk of the type of name of this region.
	 * 
	 * @return int
	 */
	public function getFkTncc() : int
	{
		return $this->_fkTncc;
	}
	
	/**
	 * Sets the name of this region.
	 * 
	 * @param string $ncc
	 * @return ApiFrInseeCogRegionInterface
	 */
	public function setNcc(string $ncc) : ApiFrInseeCogRegionInterface
	{
		$this->_ncc = $ncc;
		
		return $this;
	}
	
	/**
	 * Gets the name of this region.
	 * 
	 * @return string
	 */
	public function getNcc() : string
	{
		return $this->_ncc;
	}
	
	/**
	 * Sets the enriched name of this region.
	 * 
	 * @param string $nccenr
	 * @return ApiFrInseeCogRegionInterface
	 */
	public function setNccenr(string $nccenr) : ApiFrInseeCogRegionInterface
	{
		$this->_nccenr = $nccenr;
		
		return $this;
	}
	
	/**
	 * Gets the enriched name of this region.
	 * 
	 * @return string
	 */
	public function getNccenr() : string
	{
		return $this->_nccenr;
	}
	
}
