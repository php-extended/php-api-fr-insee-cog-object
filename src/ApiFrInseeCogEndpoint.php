<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog;

use ArrayIterator;
use Iterator;
use PhpExtended\DataProvider\CsvFileDataIterator;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierInterface;

/**
 * InseeCogEndpoint class file.
 * 
 * This class is a simple implementation based on local csv files of the 
 * InseeCogEndpointInterface.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 * @SuppressWarnings("PHPMD.CamelCaseVariableName")
 */
class ApiFrInseeCogEndpoint implements ApiFrInseeCogEndpointInterface
{
	
	/**
	 * Cache for the actualite pays values.
	 *
	 * @var array<integer, ApiFrInseeCogActualitePays>
	 */
	protected static array $_actualitePays = [];
	
	/**
	 * Cache for the composition cantonale values.
	 *
	 * @var array<integer, ApiFrInseeCogCompositionCantonale>
	 */
	protected static array $_compositionCantonales = [];
	
	/**
	 * Cache for the tncc values.
	 *
	 * @var array<integer, ApiFrInseeCogTncc>
	 */
	protected static array $_tnccs = [];
	
	/**
	 * Cache for the type cantons.
	 *
	 * @var array<string, ApiFrInseeCogTypeCanton>
	 */
	protected static array $_typeCantons = [];
	
	/**
	 * Cache for the type communes.
	 *
	 * @var array<string, ApiFrInseeCogTypeCommune>
	 */
	protected static array $_typeCommunes = [];
	
	/**
	 * Cache for the type event communes.
	 *
	 * @var array<integer, ApiFrInseeCogTypeEventCommune>
	 */
	protected static array $_typeEventCommunes = [];
	
	/**
	 * The reifier.
	 * 
	 * @var ?ReifierInterface
	 */
	protected ?ReifierInterface $_reifier = null;
	
	/**
	 * Builds a new InseeCogEndpoint with its dependancies.
	 * 
	 * @param ?ReifierInterface $reifier
	 */
	public function __construct(?ReifierInterface $reifier = null)
	{
		$this->_reifier = $reifier;
		$config = $this->getReifier()->getConfiguration();
		$config->addFieldAllowedToFail(ApiFrInseeCogPaysHistory::class, 'date_end');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface::getMinimumAvailableYear()
	 */
	public function getMinimumAvailableYear() : int
	{
		return 1999;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface::getMaximumAvailableYear()
	 */
	public function getMaximumAvailableYear() : int
	{
		return 2024;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface::getPaysIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getPaysIterator() : Iterator
	{
		$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/pays.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeCogPays::class, $iterator);
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface::getCommuneIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getPaysHistoryIterator() : Iterator
	{
		$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/pays_history.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeCogPaysHistory::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface::getRegionIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getRegionIterator(int $year) : Iterator
	{
		$filePath = \dirname(__DIR__).'/data/'.((string) $year).'/region.csv';
		if(!\is_file($filePath))
		{
			return new ArrayIterator();
		}
		$iterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeCogRegion::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface::getDepartementIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getDepartementIterator(int $year) : Iterator
	{
		$filePath = \dirname(__DIR__).'/data/'.((string) $year).'/departement.csv';
		if(!\is_file($filePath))
		{
			return new ArrayIterator();
		}
		$iterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeCogDepartement::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface::getArrondissementIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getArrondissementIterator(int $year) : Iterator
	{
		$filePath = \dirname(__DIR__).'/data/'.((string) $year).'/arrondissement.csv';
		if(!\is_file($filePath))
		{
			return new ArrayIterator();
		}
		$iterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeCogArrondissement::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface::getCantonIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCantonIterator(int $year) : Iterator
	{
		$filePath = \dirname(__DIR__).'/data/'.((string) $year).'/canton.csv';
		if(!\is_file($filePath))
		{
			return new ArrayIterator();
		}
		$iterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeCogCanton::class, $iterator);
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface::getCollectiviteTerritorialeIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCollectiviteTerritorialeIterator(int $year) : Iterator
	{
		$filePath = \dirname(__DIR__).'/data/'.((string) $year).'/collectivites_territoriales.csv';
		if(!\is_file($filePath))
		{
			return new ArrayIterator();
		}
		$iterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeCogCollectiviteTerritoriale::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface::getCommuneIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCommuneIterator(int $year) : Iterator
	{
		$filePath = \dirname(__DIR__).'/data/'.((string) $year).'/commune.csv';
		if(!\is_file($filePath))
		{
			return new ArrayIterator();
		}
		$iterator = new CsvFileDataIterator($filePath, true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeCogCommune::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface::getEventCommuneIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getEventCommuneIterator() : Iterator
	{
		$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/event_commune.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
		
		return $this->getReifier()->reifyIterator(ApiFrInseeCogEventCommune::class, $iterator);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface::getActualitePaysIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getActualitePaysIterator() : Iterator
	{
		if(empty(self::$_actualitePays))
		{
			self::$_actualitePays = [];
			$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/actualite_pays.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
			$iterator = $this->getReifier()->reifyIterator(ApiFrInseeCogActualitePays::class, $iterator);
			
			/** @var ApiFrInseeCogActualitePays $value */
			foreach($iterator as $value)
			{
				self::$_actualitePays[(int) $value->getId()] = $value;
			}
		}
		
		return new ArrayIterator(self::$_actualitePays);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface::getActualitePays()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getActualitePays(?int $id) : ?ApiFrInseeCogActualitePays
	{
		$this->getActualitePaysIterator();
		
		return self::$_actualitePays[(int) $id] ?? null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface::getCompositionCantonaleIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCompositionCantonaleIterator() : Iterator
	{
		if(empty(self::$_compositionCantonales))
		{
			self::$_compositionCantonales = [];
			$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/composition_cantonale.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
			$iterator = $this->getReifier()->reifyIterator(ApiFrInseeCogCompositionCantonale::class, $iterator);
			
			/** @var ApiFrInseeCogCompositionCantonale $value */
			foreach($iterator as $value)
			{
				self::$_compositionCantonales[(int) $value->getId()] = $value;
			}
		}
		
		return new ArrayIterator(self::$_compositionCantonales);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface::getCompositionCantonale()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getCompositionCantonale(?int $id) : ?ApiFrInseeCogCompositionCantonale
	{
		$this->getCompositionCantonaleIterator();
		
		return self::$_compositionCantonales[(int) $id] ?? null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface::getTnccIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getTnccIterator() : Iterator
	{
		if(empty(self::$_tnccs))
		{
			self::$_tnccs = [];
			$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/tncc.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
			$iterator = $this->getReifier()->reifyIterator(ApiFrInseeCogTncc::class, $iterator);
			
			/** @var ApiFrInseeCogTncc $value */
			foreach($iterator as $value)
			{
				self::$_tnccs[(int) $value->getId()] = $value;
			}
		}
		
		return new ArrayIterator(self::$_tnccs);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface::getTncc()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getTncc(?int $id) : ?ApiFrInseeCogTncc
	{
		$this->getTnccIterator();
		
		return self::$_tnccs[(int) $id] ?? null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface::getTypeCantonIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getTypeCantonIterator() : Iterator
	{
		if(empty(self::$_typeCantons))
		{
			self::$_typeCantons = [];
			$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/type_canton.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
			$iterator = $this->getReifier()->reifyIterator(ApiFrInseeCogTypeCanton::class, $iterator);
			
			/** @var ApiFrInseeCogTypeCanton $value */
			foreach($iterator as $value)
			{
				self::$_typeCantons[(string) $value->getId()] = $value;
			}
		}
		
		return new ArrayIterator(self::$_typeCantons);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface::getTypeCanton()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getTypeCanton(?string $id) : ?ApiFrInseeCogTypeCanton
	{
		$this->getTypeCantonIterator();
		
		return self::$_typeCantons[(string) $id] ?? null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface::getTypeCommuneIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getTypeCommuneIterator() : Iterator
	{
		if(empty(self::$_typeCommunes))
		{
			self::$_typeCommunes = [];
			$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/type_commune.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
			$iterator = $this->getReifier()->reifyIterator(ApiFrInseeCogTypeCommune::class, $iterator);
			
			/** @var ApiFrInseeCogTypeCommune $value */
			foreach($iterator as $value)
			{
				self::$_typeCommunes[(string) $value->getId()] = $value;
			}
		}
		
		return new ArrayIterator(self::$_typeCommunes);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface::getTypeCommune()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getTypeCommune(?string $id) : ?ApiFrInseeCogTypeCommune
	{
		$this->getTypeCommuneIterator();
		
		return self::$_typeCommunes[(string) $id] ?? null;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface::getTypeEventCommuneIterator()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getTypeEventCommuneIterator() : Iterator
	{
		if(empty(self::$_typeEventCommunes))
		{
			self::$_typeEventCommunes = [];
			$iterator = new CsvFileDataIterator(\dirname(__DIR__).'/data/type_event_commune.csv', true, ',', '"', '\\', 'UTF-8', 'UTF-8');
			$iterator = $this->getReifier()->reifyIterator(ApiFrInseeCogTypeEventCommune::class, $iterator);
			
			/** @var ApiFrInseeCogTypeEventCommune $value */
			foreach($iterator as $value)
			{
				self::$_typeEventCommunes[(int) $value->getId()] = $value;
			}
		}
		
		return new ArrayIterator(self::$_typeEventCommunes);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrInseeCog\ApiFrInseeCogEndpointInterface::getTypeEventCommune()
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getTypeEventCommune(?int $id) : ?ApiFrInseeCogTypeEventCommune
	{
		$this->getTypeEventCommuneIterator();
		
		return self::$_typeEventCommunes[(int) $id] ?? null;
	}
	
	/**
	 * Gets the reifier.
	 * 
	 * @return ReifierInterface
	 */
	protected function getReifier() : ReifierInterface
	{
		if(null === $this->_reifier)
		{
			$this->_reifier = new Reifier();
		}
		
		return $this->_reifier;
	}
	
}
