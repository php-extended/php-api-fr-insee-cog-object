<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog;

/**
 * ApiFrInseeCogDepartement class file.
 * 
 * This is a simple implementation of the ApiFrInseeCogDepartementInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrInseeCogDepartement implements ApiFrInseeCogDepartementInterface
{
	
	/**
	 * The id of this departement.
	 * 
	 * @var string
	 */
	protected string $_id;
	
	/**
	 * The fk of the related region.
	 * 
	 * @var string
	 */
	protected string $_fkRegion;
	
	/**
	 * The fk of the related commune as cheflieu.
	 * 
	 * @var string
	 */
	protected string $_fkCommuneCheflieu;
	
	/**
	 * The fk of the type of name of this departement.
	 * 
	 * @var int
	 */
	protected int $_fkTncc;
	
	/**
	 * The name of this departement.
	 * 
	 * @var string
	 */
	protected string $_ncc;
	
	/**
	 * The enriched name of this departement.
	 * 
	 * @var string
	 */
	protected string $_nccenr;
	
	/**
	 * Constructor for ApiFrInseeCogDepartement with private members.
	 * 
	 * @param string $id
	 * @param string $fkRegion
	 * @param string $fkCommuneCheflieu
	 * @param int $fkTncc
	 * @param string $ncc
	 * @param string $nccenr
	 */
	public function __construct(string $id, string $fkRegion, string $fkCommuneCheflieu, int $fkTncc, string $ncc, string $nccenr)
	{
		$this->setId($id);
		$this->setFkRegion($fkRegion);
		$this->setFkCommuneCheflieu($fkCommuneCheflieu);
		$this->setFkTncc($fkTncc);
		$this->setNcc($ncc);
		$this->setNccenr($nccenr);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of this departement.
	 * 
	 * @param string $id
	 * @return ApiFrInseeCogDepartementInterface
	 */
	public function setId(string $id) : ApiFrInseeCogDepartementInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of this departement.
	 * 
	 * @return string
	 */
	public function getId() : string
	{
		return $this->_id;
	}
	
	/**
	 * Sets the fk of the related region.
	 * 
	 * @param string $fkRegion
	 * @return ApiFrInseeCogDepartementInterface
	 */
	public function setFkRegion(string $fkRegion) : ApiFrInseeCogDepartementInterface
	{
		$this->_fkRegion = $fkRegion;
		
		return $this;
	}
	
	/**
	 * Gets the fk of the related region.
	 * 
	 * @return string
	 */
	public function getFkRegion() : string
	{
		return $this->_fkRegion;
	}
	
	/**
	 * Sets the fk of the related commune as cheflieu.
	 * 
	 * @param string $fkCommuneCheflieu
	 * @return ApiFrInseeCogDepartementInterface
	 */
	public function setFkCommuneCheflieu(string $fkCommuneCheflieu) : ApiFrInseeCogDepartementInterface
	{
		$this->_fkCommuneCheflieu = $fkCommuneCheflieu;
		
		return $this;
	}
	
	/**
	 * Gets the fk of the related commune as cheflieu.
	 * 
	 * @return string
	 */
	public function getFkCommuneCheflieu() : string
	{
		return $this->_fkCommuneCheflieu;
	}
	
	/**
	 * Sets the fk of the type of name of this departement.
	 * 
	 * @param int $fkTncc
	 * @return ApiFrInseeCogDepartementInterface
	 */
	public function setFkTncc(int $fkTncc) : ApiFrInseeCogDepartementInterface
	{
		$this->_fkTncc = $fkTncc;
		
		return $this;
	}
	
	/**
	 * Gets the fk of the type of name of this departement.
	 * 
	 * @return int
	 */
	public function getFkTncc() : int
	{
		return $this->_fkTncc;
	}
	
	/**
	 * Sets the name of this departement.
	 * 
	 * @param string $ncc
	 * @return ApiFrInseeCogDepartementInterface
	 */
	public function setNcc(string $ncc) : ApiFrInseeCogDepartementInterface
	{
		$this->_ncc = $ncc;
		
		return $this;
	}
	
	/**
	 * Gets the name of this departement.
	 * 
	 * @return string
	 */
	public function getNcc() : string
	{
		return $this->_ncc;
	}
	
	/**
	 * Sets the enriched name of this departement.
	 * 
	 * @param string $nccenr
	 * @return ApiFrInseeCogDepartementInterface
	 */
	public function setNccenr(string $nccenr) : ApiFrInseeCogDepartementInterface
	{
		$this->_nccenr = $nccenr;
		
		return $this;
	}
	
	/**
	 * Gets the enriched name of this departement.
	 * 
	 * @return string
	 */
	public function getNccenr() : string
	{
		return $this->_nccenr;
	}
	
}
