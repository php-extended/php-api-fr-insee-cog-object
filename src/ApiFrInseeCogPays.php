<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog;

/**
 * ApiFrInseeCogPays class file.
 * 
 * This is a simple implementation of the ApiFrInseeCogPaysInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrInseeCogPays implements ApiFrInseeCogPaysInterface
{
	
	/**
	 * The id of this country.
	 * 
	 * @var int
	 */
	protected int $_id;
	
	/**
	 * The fk of the related actual country.
	 * 
	 * @var int
	 */
	protected int $_fkActualitePays;
	
	/**
	 * The fk of the current parent country, if any.
	 * 
	 * @var ?int
	 */
	protected ?int $_fkPaysParent = null;
	
	/**
	 * The year of creation of this country, if after 1943.
	 * 
	 * @var ?int
	 */
	protected ?int $_creationYear = null;
	
	/**
	 * The name of this country used in the COG.
	 * 
	 * @var string
	 */
	protected string $_libCog;
	
	/**
	 * Official name of this country.
	 * 
	 * @var string
	 */
	protected string $_libEnr;
	
	/**
	 * The code iso bi-alpha, if any.
	 * 
	 * @var ?string
	 */
	protected ?string $_iso2 = null;
	
	/**
	 * The code iso tri-alpha, if any.
	 * 
	 * @var ?string
	 */
	protected ?string $_iso3 = null;
	
	/**
	 * The code iso tri-alphanumeric, if any.
	 * 
	 * @var ?string
	 */
	protected ?string $_isonum3 = null;
	
	/**
	 * Constructor for ApiFrInseeCogPays with private members.
	 * 
	 * @param int $id
	 * @param int $fkActualitePays
	 * @param string $libCog
	 * @param string $libEnr
	 */
	public function __construct(int $id, int $fkActualitePays, string $libCog, string $libEnr)
	{
		$this->setId($id);
		$this->setFkActualitePays($fkActualitePays);
		$this->setLibCog($libCog);
		$this->setLibEnr($libEnr);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of this country.
	 * 
	 * @param int $id
	 * @return ApiFrInseeCogPaysInterface
	 */
	public function setId(int $id) : ApiFrInseeCogPaysInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of this country.
	 * 
	 * @return int
	 */
	public function getId() : int
	{
		return $this->_id;
	}
	
	/**
	 * Sets the fk of the related actual country.
	 * 
	 * @param int $fkActualitePays
	 * @return ApiFrInseeCogPaysInterface
	 */
	public function setFkActualitePays(int $fkActualitePays) : ApiFrInseeCogPaysInterface
	{
		$this->_fkActualitePays = $fkActualitePays;
		
		return $this;
	}
	
	/**
	 * Gets the fk of the related actual country.
	 * 
	 * @return int
	 */
	public function getFkActualitePays() : int
	{
		return $this->_fkActualitePays;
	}
	
	/**
	 * Sets the fk of the current parent country, if any.
	 * 
	 * @param ?int $fkPaysParent
	 * @return ApiFrInseeCogPaysInterface
	 */
	public function setFkPaysParent(?int $fkPaysParent) : ApiFrInseeCogPaysInterface
	{
		$this->_fkPaysParent = $fkPaysParent;
		
		return $this;
	}
	
	/**
	 * Gets the fk of the current parent country, if any.
	 * 
	 * @return ?int
	 */
	public function getFkPaysParent() : ?int
	{
		return $this->_fkPaysParent;
	}
	
	/**
	 * Sets the year of creation of this country, if after 1943.
	 * 
	 * @param ?int $creationYear
	 * @return ApiFrInseeCogPaysInterface
	 */
	public function setCreationYear(?int $creationYear) : ApiFrInseeCogPaysInterface
	{
		$this->_creationYear = $creationYear;
		
		return $this;
	}
	
	/**
	 * Gets the year of creation of this country, if after 1943.
	 * 
	 * @return ?int
	 */
	public function getCreationYear() : ?int
	{
		return $this->_creationYear;
	}
	
	/**
	 * Sets the name of this country used in the COG.
	 * 
	 * @param string $libCog
	 * @return ApiFrInseeCogPaysInterface
	 */
	public function setLibCog(string $libCog) : ApiFrInseeCogPaysInterface
	{
		$this->_libCog = $libCog;
		
		return $this;
	}
	
	/**
	 * Gets the name of this country used in the COG.
	 * 
	 * @return string
	 */
	public function getLibCog() : string
	{
		return $this->_libCog;
	}
	
	/**
	 * Sets official name of this country.
	 * 
	 * @param string $libEnr
	 * @return ApiFrInseeCogPaysInterface
	 */
	public function setLibEnr(string $libEnr) : ApiFrInseeCogPaysInterface
	{
		$this->_libEnr = $libEnr;
		
		return $this;
	}
	
	/**
	 * Gets official name of this country.
	 * 
	 * @return string
	 */
	public function getLibEnr() : string
	{
		return $this->_libEnr;
	}
	
	/**
	 * Sets the code iso bi-alpha, if any.
	 * 
	 * @param ?string $iso2
	 * @return ApiFrInseeCogPaysInterface
	 */
	public function setIso2(?string $iso2) : ApiFrInseeCogPaysInterface
	{
		$this->_iso2 = $iso2;
		
		return $this;
	}
	
	/**
	 * Gets the code iso bi-alpha, if any.
	 * 
	 * @return ?string
	 */
	public function getIso2() : ?string
	{
		return $this->_iso2;
	}
	
	/**
	 * Sets the code iso tri-alpha, if any.
	 * 
	 * @param ?string $iso3
	 * @return ApiFrInseeCogPaysInterface
	 */
	public function setIso3(?string $iso3) : ApiFrInseeCogPaysInterface
	{
		$this->_iso3 = $iso3;
		
		return $this;
	}
	
	/**
	 * Gets the code iso tri-alpha, if any.
	 * 
	 * @return ?string
	 */
	public function getIso3() : ?string
	{
		return $this->_iso3;
	}
	
	/**
	 * Sets the code iso tri-alphanumeric, if any.
	 * 
	 * @param ?string $isonum3
	 * @return ApiFrInseeCogPaysInterface
	 */
	public function setIsonum3(?string $isonum3) : ApiFrInseeCogPaysInterface
	{
		$this->_isonum3 = $isonum3;
		
		return $this;
	}
	
	/**
	 * Gets the code iso tri-alphanumeric, if any.
	 * 
	 * @return ?string
	 */
	public function getIsonum3() : ?string
	{
		return $this->_isonum3;
	}
	
}
