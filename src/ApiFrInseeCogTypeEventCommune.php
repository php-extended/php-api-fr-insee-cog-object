<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog;

/**
 * ApiFrInseeCogTypeEventCommune class file.
 * 
 * This is a simple implementation of the
 * ApiFrInseeCogTypeEventCommuneInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrInseeCogTypeEventCommune implements ApiFrInseeCogTypeEventCommuneInterface
{
	
	/**
	 * The id of this type event.
	 * 
	 * @var string
	 */
	protected string $_id;
	
	/**
	 * The libelle of this type event.
	 * 
	 * @var string
	 */
	protected string $_libelle;
	
	/**
	 * Constructor for ApiFrInseeCogTypeEventCommune with private members.
	 * 
	 * @param string $id
	 * @param string $libelle
	 */
	public function __construct(string $id, string $libelle)
	{
		$this->setId($id);
		$this->setLibelle($libelle);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of this type event.
	 * 
	 * @param string $id
	 * @return ApiFrInseeCogTypeEventCommuneInterface
	 */
	public function setId(string $id) : ApiFrInseeCogTypeEventCommuneInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of this type event.
	 * 
	 * @return string
	 */
	public function getId() : string
	{
		return $this->_id;
	}
	
	/**
	 * Sets the libelle of this type event.
	 * 
	 * @param string $libelle
	 * @return ApiFrInseeCogTypeEventCommuneInterface
	 */
	public function setLibelle(string $libelle) : ApiFrInseeCogTypeEventCommuneInterface
	{
		$this->_libelle = $libelle;
		
		return $this;
	}
	
	/**
	 * Gets the libelle of this type event.
	 * 
	 * @return string
	 */
	public function getLibelle() : string
	{
		return $this->_libelle;
	}
	
}
