<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog;

use DateTimeInterface;

/**
 * ApiFrInseeCogEventCommune class file.
 * 
 * This is a simple implementation of the ApiFrInseeCogEventCommuneInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrInseeCogEventCommune implements ApiFrInseeCogEventCommuneInterface
{
	
	/**
	 * The date of the event.
	 * 
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_dateEffet;
	
	/**
	 * The id of the related type of event.
	 * 
	 * @var int
	 */
	protected int $_fkTypeEventCommune;
	
	/**
	 * The id of the type of commune before the event.
	 * 
	 * @var string
	 */
	protected string $_fkTypeCommuneBefore;
	
	/**
	 * The id of the commune before the event.
	 * 
	 * @var string
	 */
	protected string $_fkCommuneBefore;
	
	/**
	 * The id of the type of commune after the event.
	 * 
	 * @var string
	 */
	protected string $_fkTypeCommuneAfter;
	
	/**
	 * The id of the commune after the event.
	 * 
	 * @var string
	 */
	protected string $_fkCommuneAfter;
	
	/**
	 * The type of name of the commune before the event.
	 * 
	 * @var int
	 */
	protected int $_fkTnccBefore;
	
	/**
	 * The name of the commune before the event.
	 * 
	 * @var string
	 */
	protected string $_nccBefore;
	
	/**
	 * The enriched name of the commune before the event.
	 * 
	 * @var string
	 */
	protected string $_nccenrBefore;
	
	/**
	 * The type of name of the commune after the event.
	 * 
	 * @var int
	 */
	protected int $_fkTnccAfter;
	
	/**
	 * The name of the commune after the event.
	 * 
	 * @var string
	 */
	protected string $_nccAfter;
	
	/**
	 * The enriched name of the commune after the event.
	 * 
	 * @var string
	 */
	protected string $_nccenrAfter;
	
	/**
	 * Constructor for ApiFrInseeCogEventCommune with private members.
	 * 
	 * @param DateTimeInterface $dateEffet
	 * @param int $fkTypeEventCommune
	 * @param string $fkTypeCommuneBefore
	 * @param string $fkCommuneBefore
	 * @param string $fkTypeCommuneAfter
	 * @param string $fkCommuneAfter
	 * @param int $fkTnccBefore
	 * @param string $nccBefore
	 * @param string $nccenrBefore
	 * @param int $fkTnccAfter
	 * @param string $nccAfter
	 * @param string $nccenrAfter
	 * @SuppressWarnings("PHPMD.ExcessiveParameterList")
	 */
	public function __construct(DateTimeInterface $dateEffet, int $fkTypeEventCommune, string $fkTypeCommuneBefore, string $fkCommuneBefore, string $fkTypeCommuneAfter, string $fkCommuneAfter, int $fkTnccBefore, string $nccBefore, string $nccenrBefore, int $fkTnccAfter, string $nccAfter, string $nccenrAfter)
	{
		$this->setDateEffet($dateEffet);
		$this->setFkTypeEventCommune($fkTypeEventCommune);
		$this->setFkTypeCommuneBefore($fkTypeCommuneBefore);
		$this->setFkCommuneBefore($fkCommuneBefore);
		$this->setFkTypeCommuneAfter($fkTypeCommuneAfter);
		$this->setFkCommuneAfter($fkCommuneAfter);
		$this->setFkTnccBefore($fkTnccBefore);
		$this->setNccBefore($nccBefore);
		$this->setNccenrBefore($nccenrBefore);
		$this->setFkTnccAfter($fkTnccAfter);
		$this->setNccAfter($nccAfter);
		$this->setNccenrAfter($nccenrAfter);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the date of the event.
	 * 
	 * @param DateTimeInterface $dateEffet
	 * @return ApiFrInseeCogEventCommuneInterface
	 */
	public function setDateEffet(DateTimeInterface $dateEffet) : ApiFrInseeCogEventCommuneInterface
	{
		$this->_dateEffet = $dateEffet;
		
		return $this;
	}
	
	/**
	 * Gets the date of the event.
	 * 
	 * @return DateTimeInterface
	 */
	public function getDateEffet() : DateTimeInterface
	{
		return $this->_dateEffet;
	}
	
	/**
	 * Sets the id of the related type of event.
	 * 
	 * @param int $fkTypeEventCommune
	 * @return ApiFrInseeCogEventCommuneInterface
	 */
	public function setFkTypeEventCommune(int $fkTypeEventCommune) : ApiFrInseeCogEventCommuneInterface
	{
		$this->_fkTypeEventCommune = $fkTypeEventCommune;
		
		return $this;
	}
	
	/**
	 * Gets the id of the related type of event.
	 * 
	 * @return int
	 */
	public function getFkTypeEventCommune() : int
	{
		return $this->_fkTypeEventCommune;
	}
	
	/**
	 * Sets the id of the type of commune before the event.
	 * 
	 * @param string $fkTypeCommuneBefore
	 * @return ApiFrInseeCogEventCommuneInterface
	 */
	public function setFkTypeCommuneBefore(string $fkTypeCommuneBefore) : ApiFrInseeCogEventCommuneInterface
	{
		$this->_fkTypeCommuneBefore = $fkTypeCommuneBefore;
		
		return $this;
	}
	
	/**
	 * Gets the id of the type of commune before the event.
	 * 
	 * @return string
	 */
	public function getFkTypeCommuneBefore() : string
	{
		return $this->_fkTypeCommuneBefore;
	}
	
	/**
	 * Sets the id of the commune before the event.
	 * 
	 * @param string $fkCommuneBefore
	 * @return ApiFrInseeCogEventCommuneInterface
	 */
	public function setFkCommuneBefore(string $fkCommuneBefore) : ApiFrInseeCogEventCommuneInterface
	{
		$this->_fkCommuneBefore = $fkCommuneBefore;
		
		return $this;
	}
	
	/**
	 * Gets the id of the commune before the event.
	 * 
	 * @return string
	 */
	public function getFkCommuneBefore() : string
	{
		return $this->_fkCommuneBefore;
	}
	
	/**
	 * Sets the id of the type of commune after the event.
	 * 
	 * @param string $fkTypeCommuneAfter
	 * @return ApiFrInseeCogEventCommuneInterface
	 */
	public function setFkTypeCommuneAfter(string $fkTypeCommuneAfter) : ApiFrInseeCogEventCommuneInterface
	{
		$this->_fkTypeCommuneAfter = $fkTypeCommuneAfter;
		
		return $this;
	}
	
	/**
	 * Gets the id of the type of commune after the event.
	 * 
	 * @return string
	 */
	public function getFkTypeCommuneAfter() : string
	{
		return $this->_fkTypeCommuneAfter;
	}
	
	/**
	 * Sets the id of the commune after the event.
	 * 
	 * @param string $fkCommuneAfter
	 * @return ApiFrInseeCogEventCommuneInterface
	 */
	public function setFkCommuneAfter(string $fkCommuneAfter) : ApiFrInseeCogEventCommuneInterface
	{
		$this->_fkCommuneAfter = $fkCommuneAfter;
		
		return $this;
	}
	
	/**
	 * Gets the id of the commune after the event.
	 * 
	 * @return string
	 */
	public function getFkCommuneAfter() : string
	{
		return $this->_fkCommuneAfter;
	}
	
	/**
	 * Sets the type of name of the commune before the event.
	 * 
	 * @param int $fkTnccBefore
	 * @return ApiFrInseeCogEventCommuneInterface
	 */
	public function setFkTnccBefore(int $fkTnccBefore) : ApiFrInseeCogEventCommuneInterface
	{
		$this->_fkTnccBefore = $fkTnccBefore;
		
		return $this;
	}
	
	/**
	 * Gets the type of name of the commune before the event.
	 * 
	 * @return int
	 */
	public function getFkTnccBefore() : int
	{
		return $this->_fkTnccBefore;
	}
	
	/**
	 * Sets the name of the commune before the event.
	 * 
	 * @param string $nccBefore
	 * @return ApiFrInseeCogEventCommuneInterface
	 */
	public function setNccBefore(string $nccBefore) : ApiFrInseeCogEventCommuneInterface
	{
		$this->_nccBefore = $nccBefore;
		
		return $this;
	}
	
	/**
	 * Gets the name of the commune before the event.
	 * 
	 * @return string
	 */
	public function getNccBefore() : string
	{
		return $this->_nccBefore;
	}
	
	/**
	 * Sets the enriched name of the commune before the event.
	 * 
	 * @param string $nccenrBefore
	 * @return ApiFrInseeCogEventCommuneInterface
	 */
	public function setNccenrBefore(string $nccenrBefore) : ApiFrInseeCogEventCommuneInterface
	{
		$this->_nccenrBefore = $nccenrBefore;
		
		return $this;
	}
	
	/**
	 * Gets the enriched name of the commune before the event.
	 * 
	 * @return string
	 */
	public function getNccenrBefore() : string
	{
		return $this->_nccenrBefore;
	}
	
	/**
	 * Sets the type of name of the commune after the event.
	 * 
	 * @param int $fkTnccAfter
	 * @return ApiFrInseeCogEventCommuneInterface
	 */
	public function setFkTnccAfter(int $fkTnccAfter) : ApiFrInseeCogEventCommuneInterface
	{
		$this->_fkTnccAfter = $fkTnccAfter;
		
		return $this;
	}
	
	/**
	 * Gets the type of name of the commune after the event.
	 * 
	 * @return int
	 */
	public function getFkTnccAfter() : int
	{
		return $this->_fkTnccAfter;
	}
	
	/**
	 * Sets the name of the commune after the event.
	 * 
	 * @param string $nccAfter
	 * @return ApiFrInseeCogEventCommuneInterface
	 */
	public function setNccAfter(string $nccAfter) : ApiFrInseeCogEventCommuneInterface
	{
		$this->_nccAfter = $nccAfter;
		
		return $this;
	}
	
	/**
	 * Gets the name of the commune after the event.
	 * 
	 * @return string
	 */
	public function getNccAfter() : string
	{
		return $this->_nccAfter;
	}
	
	/**
	 * Sets the enriched name of the commune after the event.
	 * 
	 * @param string $nccenrAfter
	 * @return ApiFrInseeCogEventCommuneInterface
	 */
	public function setNccenrAfter(string $nccenrAfter) : ApiFrInseeCogEventCommuneInterface
	{
		$this->_nccenrAfter = $nccenrAfter;
		
		return $this;
	}
	
	/**
	 * Gets the enriched name of the commune after the event.
	 * 
	 * @return string
	 */
	public function getNccenrAfter() : string
	{
		return $this->_nccenrAfter;
	}
	
}
