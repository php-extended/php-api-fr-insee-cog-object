<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog;

/**
 * ApiFrInseeCogCanton class file.
 * 
 * This is a simple implementation of the ApiFrInseeCogCantonInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrInseeCogCanton implements ApiFrInseeCogCantonInterface
{
	
	/**
	 * The id of this canton.
	 * 
	 * @var string
	 */
	protected string $_id;
	
	/**
	 * The fk of the related arrondissement.
	 * 
	 * @var string
	 */
	protected string $_fkArrondissement;
	
	/**
	 * The fk of the related type of canton.
	 * 
	 * @var string
	 */
	protected string $_fkTypeCanton;
	
	/**
	 * The fk of the related composition cantonale.
	 * 
	 * @var int
	 */
	protected int $_fkCompositionCantonale;
	
	/**
	 * The fk of the related commune as cheflieu.
	 * 
	 * @var string
	 */
	protected string $_fkCommuneCheflieu;
	
	/**
	 * The type of name of this canton.
	 * 
	 * @var int
	 */
	protected int $_fkTncc;
	
	/**
	 * The name of this canton.
	 * 
	 * @var string
	 */
	protected string $_ncc;
	
	/**
	 * The enriched name of this canton.
	 * 
	 * @var string
	 */
	protected string $_nccenr;
	
	/**
	 * Constructor for ApiFrInseeCogCanton with private members.
	 * 
	 * @param string $id
	 * @param string $fkArrondissement
	 * @param string $fkTypeCanton
	 * @param int $fkCompositionCantonale
	 * @param string $fkCommuneCheflieu
	 * @param int $fkTncc
	 * @param string $ncc
	 * @param string $nccenr
	 */
	public function __construct(string $id, string $fkArrondissement, string $fkTypeCanton, int $fkCompositionCantonale, string $fkCommuneCheflieu, int $fkTncc, string $ncc, string $nccenr)
	{
		$this->setId($id);
		$this->setFkArrondissement($fkArrondissement);
		$this->setFkTypeCanton($fkTypeCanton);
		$this->setFkCompositionCantonale($fkCompositionCantonale);
		$this->setFkCommuneCheflieu($fkCommuneCheflieu);
		$this->setFkTncc($fkTncc);
		$this->setNcc($ncc);
		$this->setNccenr($nccenr);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of this canton.
	 * 
	 * @param string $id
	 * @return ApiFrInseeCogCantonInterface
	 */
	public function setId(string $id) : ApiFrInseeCogCantonInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of this canton.
	 * 
	 * @return string
	 */
	public function getId() : string
	{
		return $this->_id;
	}
	
	/**
	 * Sets the fk of the related arrondissement.
	 * 
	 * @param string $fkArrondissement
	 * @return ApiFrInseeCogCantonInterface
	 */
	public function setFkArrondissement(string $fkArrondissement) : ApiFrInseeCogCantonInterface
	{
		$this->_fkArrondissement = $fkArrondissement;
		
		return $this;
	}
	
	/**
	 * Gets the fk of the related arrondissement.
	 * 
	 * @return string
	 */
	public function getFkArrondissement() : string
	{
		return $this->_fkArrondissement;
	}
	
	/**
	 * Sets the fk of the related type of canton.
	 * 
	 * @param string $fkTypeCanton
	 * @return ApiFrInseeCogCantonInterface
	 */
	public function setFkTypeCanton(string $fkTypeCanton) : ApiFrInseeCogCantonInterface
	{
		$this->_fkTypeCanton = $fkTypeCanton;
		
		return $this;
	}
	
	/**
	 * Gets the fk of the related type of canton.
	 * 
	 * @return string
	 */
	public function getFkTypeCanton() : string
	{
		return $this->_fkTypeCanton;
	}
	
	/**
	 * Sets the fk of the related composition cantonale.
	 * 
	 * @param int $fkCompositionCantonale
	 * @return ApiFrInseeCogCantonInterface
	 */
	public function setFkCompositionCantonale(int $fkCompositionCantonale) : ApiFrInseeCogCantonInterface
	{
		$this->_fkCompositionCantonale = $fkCompositionCantonale;
		
		return $this;
	}
	
	/**
	 * Gets the fk of the related composition cantonale.
	 * 
	 * @return int
	 */
	public function getFkCompositionCantonale() : int
	{
		return $this->_fkCompositionCantonale;
	}
	
	/**
	 * Sets the fk of the related commune as cheflieu.
	 * 
	 * @param string $fkCommuneCheflieu
	 * @return ApiFrInseeCogCantonInterface
	 */
	public function setFkCommuneCheflieu(string $fkCommuneCheflieu) : ApiFrInseeCogCantonInterface
	{
		$this->_fkCommuneCheflieu = $fkCommuneCheflieu;
		
		return $this;
	}
	
	/**
	 * Gets the fk of the related commune as cheflieu.
	 * 
	 * @return string
	 */
	public function getFkCommuneCheflieu() : string
	{
		return $this->_fkCommuneCheflieu;
	}
	
	/**
	 * Sets the type of name of this canton.
	 * 
	 * @param int $fkTncc
	 * @return ApiFrInseeCogCantonInterface
	 */
	public function setFkTncc(int $fkTncc) : ApiFrInseeCogCantonInterface
	{
		$this->_fkTncc = $fkTncc;
		
		return $this;
	}
	
	/**
	 * Gets the type of name of this canton.
	 * 
	 * @return int
	 */
	public function getFkTncc() : int
	{
		return $this->_fkTncc;
	}
	
	/**
	 * Sets the name of this canton.
	 * 
	 * @param string $ncc
	 * @return ApiFrInseeCogCantonInterface
	 */
	public function setNcc(string $ncc) : ApiFrInseeCogCantonInterface
	{
		$this->_ncc = $ncc;
		
		return $this;
	}
	
	/**
	 * Gets the name of this canton.
	 * 
	 * @return string
	 */
	public function getNcc() : string
	{
		return $this->_ncc;
	}
	
	/**
	 * Sets the enriched name of this canton.
	 * 
	 * @param string $nccenr
	 * @return ApiFrInseeCogCantonInterface
	 */
	public function setNccenr(string $nccenr) : ApiFrInseeCogCantonInterface
	{
		$this->_nccenr = $nccenr;
		
		return $this;
	}
	
	/**
	 * Gets the enriched name of this canton.
	 * 
	 * @return string
	 */
	public function getNccenr() : string
	{
		return $this->_nccenr;
	}
	
}
