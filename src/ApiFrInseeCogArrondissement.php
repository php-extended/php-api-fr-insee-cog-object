<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog;

/**
 * ApiFrInseeCogArrondissement class file.
 * 
 * This is a simple implementation of the ApiFrInseeCogArrondissementInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrInseeCogArrondissement implements ApiFrInseeCogArrondissementInterface
{
	
	/**
	 * The id of this arrondissement.
	 * 
	 * @var string
	 */
	protected string $_id;
	
	/**
	 * The fk of the related departement.
	 * 
	 * @var string
	 */
	protected string $_fkDepartement;
	
	/**
	 * The fk of the commune as cheflieu.
	 * 
	 * @var string
	 */
	protected string $_fkCommuneCheflieu;
	
	/**
	 * The type of name of this arrondissement.
	 * 
	 * @var int
	 */
	protected int $_fkTncc;
	
	/**
	 * The name of this arrondissement.
	 * 
	 * @var string
	 */
	protected string $_ncc;
	
	/**
	 * The enriched name of this arrondissement.
	 * 
	 * @var string
	 */
	protected string $_nccenr;
	
	/**
	 * Constructor for ApiFrInseeCogArrondissement with private members.
	 * 
	 * @param string $id
	 * @param string $fkDepartement
	 * @param string $fkCommuneCheflieu
	 * @param int $fkTncc
	 * @param string $ncc
	 * @param string $nccenr
	 */
	public function __construct(string $id, string $fkDepartement, string $fkCommuneCheflieu, int $fkTncc, string $ncc, string $nccenr)
	{
		$this->setId($id);
		$this->setFkDepartement($fkDepartement);
		$this->setFkCommuneCheflieu($fkCommuneCheflieu);
		$this->setFkTncc($fkTncc);
		$this->setNcc($ncc);
		$this->setNccenr($nccenr);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of this arrondissement.
	 * 
	 * @param string $id
	 * @return ApiFrInseeCogArrondissementInterface
	 */
	public function setId(string $id) : ApiFrInseeCogArrondissementInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of this arrondissement.
	 * 
	 * @return string
	 */
	public function getId() : string
	{
		return $this->_id;
	}
	
	/**
	 * Sets the fk of the related departement.
	 * 
	 * @param string $fkDepartement
	 * @return ApiFrInseeCogArrondissementInterface
	 */
	public function setFkDepartement(string $fkDepartement) : ApiFrInseeCogArrondissementInterface
	{
		$this->_fkDepartement = $fkDepartement;
		
		return $this;
	}
	
	/**
	 * Gets the fk of the related departement.
	 * 
	 * @return string
	 */
	public function getFkDepartement() : string
	{
		return $this->_fkDepartement;
	}
	
	/**
	 * Sets the fk of the commune as cheflieu.
	 * 
	 * @param string $fkCommuneCheflieu
	 * @return ApiFrInseeCogArrondissementInterface
	 */
	public function setFkCommuneCheflieu(string $fkCommuneCheflieu) : ApiFrInseeCogArrondissementInterface
	{
		$this->_fkCommuneCheflieu = $fkCommuneCheflieu;
		
		return $this;
	}
	
	/**
	 * Gets the fk of the commune as cheflieu.
	 * 
	 * @return string
	 */
	public function getFkCommuneCheflieu() : string
	{
		return $this->_fkCommuneCheflieu;
	}
	
	/**
	 * Sets the type of name of this arrondissement.
	 * 
	 * @param int $fkTncc
	 * @return ApiFrInseeCogArrondissementInterface
	 */
	public function setFkTncc(int $fkTncc) : ApiFrInseeCogArrondissementInterface
	{
		$this->_fkTncc = $fkTncc;
		
		return $this;
	}
	
	/**
	 * Gets the type of name of this arrondissement.
	 * 
	 * @return int
	 */
	public function getFkTncc() : int
	{
		return $this->_fkTncc;
	}
	
	/**
	 * Sets the name of this arrondissement.
	 * 
	 * @param string $ncc
	 * @return ApiFrInseeCogArrondissementInterface
	 */
	public function setNcc(string $ncc) : ApiFrInseeCogArrondissementInterface
	{
		$this->_ncc = $ncc;
		
		return $this;
	}
	
	/**
	 * Gets the name of this arrondissement.
	 * 
	 * @return string
	 */
	public function getNcc() : string
	{
		return $this->_ncc;
	}
	
	/**
	 * Sets the enriched name of this arrondissement.
	 * 
	 * @param string $nccenr
	 * @return ApiFrInseeCogArrondissementInterface
	 */
	public function setNccenr(string $nccenr) : ApiFrInseeCogArrondissementInterface
	{
		$this->_nccenr = $nccenr;
		
		return $this;
	}
	
	/**
	 * Gets the enriched name of this arrondissement.
	 * 
	 * @return string
	 */
	public function getNccenr() : string
	{
		return $this->_nccenr;
	}
	
}
