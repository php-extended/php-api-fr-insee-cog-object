<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog;

use DateTimeInterface;

/**
 * ApiFrInseeCogPaysHistory class file.
 * 
 * This is a simple implementation of the ApiFrInseeCogPaysHistoryInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrInseeCogPaysHistory implements ApiFrInseeCogPaysHistoryInterface
{
	
	/**
	 * The id of the related country.
	 * 
	 * @var int
	 */
	protected int $_fkPaysId;
	
	/**
	 * The id of the related country before this record exists.
	 * 
	 * @var ?int
	 */
	protected ?int $_fkPaysBeforeId = null;
	
	/**
	 * The name of this country used in the COG.
	 * 
	 * @var string
	 */
	protected string $_libCog;
	
	/**
	 * Official name of this country.
	 * 
	 * @var string
	 */
	protected string $_libEnr;
	
	/**
	 * The start date of validity of this country.
	 * 
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_dateStart;
	
	/**
	 * The end date of validity of this country.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_dateEnd = null;
	
	/**
	 * Constructor for ApiFrInseeCogPaysHistory with private members.
	 * 
	 * @param int $fkPaysId
	 * @param string $libCog
	 * @param string $libEnr
	 * @param DateTimeInterface $dateStart
	 */
	public function __construct(int $fkPaysId, string $libCog, string $libEnr, DateTimeInterface $dateStart)
	{
		$this->setFkPaysId($fkPaysId);
		$this->setLibCog($libCog);
		$this->setLibEnr($libEnr);
		$this->setDateStart($dateStart);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the related country.
	 * 
	 * @param int $fkPaysId
	 * @return ApiFrInseeCogPaysHistoryInterface
	 */
	public function setFkPaysId(int $fkPaysId) : ApiFrInseeCogPaysHistoryInterface
	{
		$this->_fkPaysId = $fkPaysId;
		
		return $this;
	}
	
	/**
	 * Gets the id of the related country.
	 * 
	 * @return int
	 */
	public function getFkPaysId() : int
	{
		return $this->_fkPaysId;
	}
	
	/**
	 * Sets the id of the related country before this record exists.
	 * 
	 * @param ?int $fkPaysBeforeId
	 * @return ApiFrInseeCogPaysHistoryInterface
	 */
	public function setFkPaysBeforeId(?int $fkPaysBeforeId) : ApiFrInseeCogPaysHistoryInterface
	{
		$this->_fkPaysBeforeId = $fkPaysBeforeId;
		
		return $this;
	}
	
	/**
	 * Gets the id of the related country before this record exists.
	 * 
	 * @return ?int
	 */
	public function getFkPaysBeforeId() : ?int
	{
		return $this->_fkPaysBeforeId;
	}
	
	/**
	 * Sets the name of this country used in the COG.
	 * 
	 * @param string $libCog
	 * @return ApiFrInseeCogPaysHistoryInterface
	 */
	public function setLibCog(string $libCog) : ApiFrInseeCogPaysHistoryInterface
	{
		$this->_libCog = $libCog;
		
		return $this;
	}
	
	/**
	 * Gets the name of this country used in the COG.
	 * 
	 * @return string
	 */
	public function getLibCog() : string
	{
		return $this->_libCog;
	}
	
	/**
	 * Sets official name of this country.
	 * 
	 * @param string $libEnr
	 * @return ApiFrInseeCogPaysHistoryInterface
	 */
	public function setLibEnr(string $libEnr) : ApiFrInseeCogPaysHistoryInterface
	{
		$this->_libEnr = $libEnr;
		
		return $this;
	}
	
	/**
	 * Gets official name of this country.
	 * 
	 * @return string
	 */
	public function getLibEnr() : string
	{
		return $this->_libEnr;
	}
	
	/**
	 * Sets the start date of validity of this country.
	 * 
	 * @param DateTimeInterface $dateStart
	 * @return ApiFrInseeCogPaysHistoryInterface
	 */
	public function setDateStart(DateTimeInterface $dateStart) : ApiFrInseeCogPaysHistoryInterface
	{
		$this->_dateStart = $dateStart;
		
		return $this;
	}
	
	/**
	 * Gets the start date of validity of this country.
	 * 
	 * @return DateTimeInterface
	 */
	public function getDateStart() : DateTimeInterface
	{
		return $this->_dateStart;
	}
	
	/**
	 * Sets the end date of validity of this country.
	 * 
	 * @param ?DateTimeInterface $dateEnd
	 * @return ApiFrInseeCogPaysHistoryInterface
	 */
	public function setDateEnd(?DateTimeInterface $dateEnd) : ApiFrInseeCogPaysHistoryInterface
	{
		$this->_dateEnd = $dateEnd;
		
		return $this;
	}
	
	/**
	 * Gets the end date of validity of this country.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getDateEnd() : ?DateTimeInterface
	{
		return $this->_dateEnd;
	}
	
}
