<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog;

/**
 * ApiFrInseeCogCollectiviteTerritoriale class file.
 * 
 * This is a simple implementation of the
 * ApiFrInseeCogCollectiviteTerritorialeInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrInseeCogCollectiviteTerritoriale implements ApiFrInseeCogCollectiviteTerritorialeInterface
{
	
	/**
	 * The id of this collectivite territoriale.
	 * 
	 * @var string
	 */
	protected string $_id;
	
	/**
	 * The fk of the related region.
	 * 
	 * @var string
	 */
	protected string $_fkRegion;
	
	/**
	 * The fk of the related commune as cheflieu.
	 * 
	 * @var string
	 */
	protected string $_fkCommuneCheflieu;
	
	/**
	 * The fk of the type of name of this collectivite territoriale.
	 * 
	 * @var int
	 */
	protected int $_fkTncc;
	
	/**
	 * The name of this  collectivite territoriale.
	 * 
	 * @var string
	 */
	protected string $_ncc;
	
	/**
	 * The enriched name of this  collectivite territoriale.
	 * 
	 * @var string
	 */
	protected string $_nccenr;
	
	/**
	 * The libelle of this collectivite territoriale.
	 * 
	 * @var string
	 */
	protected string $_libelle;
	
	/**
	 * Constructor for ApiFrInseeCogCollectiviteTerritoriale with private members.
	 * 
	 * @param string $id
	 * @param string $fkRegion
	 * @param string $fkCommuneCheflieu
	 * @param int $fkTncc
	 * @param string $ncc
	 * @param string $nccenr
	 * @param string $libelle
	 */
	public function __construct(string $id, string $fkRegion, string $fkCommuneCheflieu, int $fkTncc, string $ncc, string $nccenr, string $libelle)
	{
		$this->setId($id);
		$this->setFkRegion($fkRegion);
		$this->setFkCommuneCheflieu($fkCommuneCheflieu);
		$this->setFkTncc($fkTncc);
		$this->setNcc($ncc);
		$this->setNccenr($nccenr);
		$this->setLibelle($libelle);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of this collectivite territoriale.
	 * 
	 * @param string $id
	 * @return ApiFrInseeCogCollectiviteTerritorialeInterface
	 */
	public function setId(string $id) : ApiFrInseeCogCollectiviteTerritorialeInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of this collectivite territoriale.
	 * 
	 * @return string
	 */
	public function getId() : string
	{
		return $this->_id;
	}
	
	/**
	 * Sets the fk of the related region.
	 * 
	 * @param string $fkRegion
	 * @return ApiFrInseeCogCollectiviteTerritorialeInterface
	 */
	public function setFkRegion(string $fkRegion) : ApiFrInseeCogCollectiviteTerritorialeInterface
	{
		$this->_fkRegion = $fkRegion;
		
		return $this;
	}
	
	/**
	 * Gets the fk of the related region.
	 * 
	 * @return string
	 */
	public function getFkRegion() : string
	{
		return $this->_fkRegion;
	}
	
	/**
	 * Sets the fk of the related commune as cheflieu.
	 * 
	 * @param string $fkCommuneCheflieu
	 * @return ApiFrInseeCogCollectiviteTerritorialeInterface
	 */
	public function setFkCommuneCheflieu(string $fkCommuneCheflieu) : ApiFrInseeCogCollectiviteTerritorialeInterface
	{
		$this->_fkCommuneCheflieu = $fkCommuneCheflieu;
		
		return $this;
	}
	
	/**
	 * Gets the fk of the related commune as cheflieu.
	 * 
	 * @return string
	 */
	public function getFkCommuneCheflieu() : string
	{
		return $this->_fkCommuneCheflieu;
	}
	
	/**
	 * Sets the fk of the type of name of this collectivite territoriale.
	 * 
	 * @param int $fkTncc
	 * @return ApiFrInseeCogCollectiviteTerritorialeInterface
	 */
	public function setFkTncc(int $fkTncc) : ApiFrInseeCogCollectiviteTerritorialeInterface
	{
		$this->_fkTncc = $fkTncc;
		
		return $this;
	}
	
	/**
	 * Gets the fk of the type of name of this collectivite territoriale.
	 * 
	 * @return int
	 */
	public function getFkTncc() : int
	{
		return $this->_fkTncc;
	}
	
	/**
	 * Sets the name of this  collectivite territoriale.
	 * 
	 * @param string $ncc
	 * @return ApiFrInseeCogCollectiviteTerritorialeInterface
	 */
	public function setNcc(string $ncc) : ApiFrInseeCogCollectiviteTerritorialeInterface
	{
		$this->_ncc = $ncc;
		
		return $this;
	}
	
	/**
	 * Gets the name of this  collectivite territoriale.
	 * 
	 * @return string
	 */
	public function getNcc() : string
	{
		return $this->_ncc;
	}
	
	/**
	 * Sets the enriched name of this  collectivite territoriale.
	 * 
	 * @param string $nccenr
	 * @return ApiFrInseeCogCollectiviteTerritorialeInterface
	 */
	public function setNccenr(string $nccenr) : ApiFrInseeCogCollectiviteTerritorialeInterface
	{
		$this->_nccenr = $nccenr;
		
		return $this;
	}
	
	/**
	 * Gets the enriched name of this  collectivite territoriale.
	 * 
	 * @return string
	 */
	public function getNccenr() : string
	{
		return $this->_nccenr;
	}
	
	/**
	 * Sets the libelle of this collectivite territoriale.
	 * 
	 * @param string $libelle
	 * @return ApiFrInseeCogCollectiviteTerritorialeInterface
	 */
	public function setLibelle(string $libelle) : ApiFrInseeCogCollectiviteTerritorialeInterface
	{
		$this->_libelle = $libelle;
		
		return $this;
	}
	
	/**
	 * Gets the libelle of this collectivite territoriale.
	 * 
	 * @return string
	 */
	public function getLibelle() : string
	{
		return $this->_libelle;
	}
	
}
