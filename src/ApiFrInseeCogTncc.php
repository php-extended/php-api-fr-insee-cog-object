<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog;

/**
 * ApiFrInseeCogTncc class file.
 * 
 * This is a simple implementation of the ApiFrInseeCogTnccInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrInseeCogTncc implements ApiFrInseeCogTnccInterface
{
	
	/**
	 * The id of type of name.
	 * 
	 * @var int
	 */
	protected int $_id;
	
	/**
	 * The article of the noun.
	 * 
	 * @var string
	 */
	protected string $_article;
	
	/**
	 * The charniere of the noun.
	 * 
	 * @var string
	 */
	protected string $_charniere;
	
	/**
	 * The space before the noun.
	 * 
	 * @var string
	 */
	protected string $_espace;
	
	/**
	 * Constructor for ApiFrInseeCogTncc with private members.
	 * 
	 * @param int $id
	 * @param string $article
	 * @param string $charniere
	 * @param string $espace
	 */
	public function __construct(int $id, string $article, string $charniere, string $espace)
	{
		$this->setId($id);
		$this->setArticle($article);
		$this->setCharniere($charniere);
		$this->setEspace($espace);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of type of name.
	 * 
	 * @param int $id
	 * @return ApiFrInseeCogTnccInterface
	 */
	public function setId(int $id) : ApiFrInseeCogTnccInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of type of name.
	 * 
	 * @return int
	 */
	public function getId() : int
	{
		return $this->_id;
	}
	
	/**
	 * Sets the article of the noun.
	 * 
	 * @param string $article
	 * @return ApiFrInseeCogTnccInterface
	 */
	public function setArticle(string $article) : ApiFrInseeCogTnccInterface
	{
		$this->_article = $article;
		
		return $this;
	}
	
	/**
	 * Gets the article of the noun.
	 * 
	 * @return string
	 */
	public function getArticle() : string
	{
		return $this->_article;
	}
	
	/**
	 * Sets the charniere of the noun.
	 * 
	 * @param string $charniere
	 * @return ApiFrInseeCogTnccInterface
	 */
	public function setCharniere(string $charniere) : ApiFrInseeCogTnccInterface
	{
		$this->_charniere = $charniere;
		
		return $this;
	}
	
	/**
	 * Gets the charniere of the noun.
	 * 
	 * @return string
	 */
	public function getCharniere() : string
	{
		return $this->_charniere;
	}
	
	/**
	 * Sets the space before the noun.
	 * 
	 * @param string $espace
	 * @return ApiFrInseeCogTnccInterface
	 */
	public function setEspace(string $espace) : ApiFrInseeCogTnccInterface
	{
		$this->_espace = $espace;
		
		return $this;
	}
	
	/**
	 * Gets the space before the noun.
	 * 
	 * @return string
	 */
	public function getEspace() : string
	{
		return $this->_espace;
	}
	
}
