<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog;

/**
 * ApiFrInseeCogCommune class file.
 * 
 * This is a simple implementation of the ApiFrInseeCogCommuneInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrInseeCogCommune implements ApiFrInseeCogCommuneInterface
{
	
	/**
	 * The id of this commune.
	 * 
	 * @var string
	 */
	protected string $_id;
	
	/**
	 * The fk of the related type commune.
	 * 
	 * @var string
	 */
	protected string $_fkTypeCommune;
	
	/**
	 * The fk of the related departement.
	 * 
	 * @var string
	 */
	protected string $_fkDepartement;
	
	/**
	 * The fk of the related parent commune, if any.
	 * 
	 * @var ?string
	 */
	protected ?string $_fkCommuneParent = null;
	
	/**
	 * The fk of the type of name of this commune.
	 * 
	 * @var int
	 */
	protected int $_fkTncc;
	
	/**
	 * The name of this commune.
	 * 
	 * @var string
	 */
	protected string $_ncc;
	
	/**
	 * The enriched name of this commune.
	 * 
	 * @var string
	 */
	protected string $_nccenr;
	
	/**
	 * Constructor for ApiFrInseeCogCommune with private members.
	 * 
	 * @param string $id
	 * @param string $fkTypeCommune
	 * @param string $fkDepartement
	 * @param int $fkTncc
	 * @param string $ncc
	 * @param string $nccenr
	 */
	public function __construct(string $id, string $fkTypeCommune, string $fkDepartement, int $fkTncc, string $ncc, string $nccenr)
	{
		$this->setId($id);
		$this->setFkTypeCommune($fkTypeCommune);
		$this->setFkDepartement($fkDepartement);
		$this->setFkTncc($fkTncc);
		$this->setNcc($ncc);
		$this->setNccenr($nccenr);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of this commune.
	 * 
	 * @param string $id
	 * @return ApiFrInseeCogCommuneInterface
	 */
	public function setId(string $id) : ApiFrInseeCogCommuneInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of this commune.
	 * 
	 * @return string
	 */
	public function getId() : string
	{
		return $this->_id;
	}
	
	/**
	 * Sets the fk of the related type commune.
	 * 
	 * @param string $fkTypeCommune
	 * @return ApiFrInseeCogCommuneInterface
	 */
	public function setFkTypeCommune(string $fkTypeCommune) : ApiFrInseeCogCommuneInterface
	{
		$this->_fkTypeCommune = $fkTypeCommune;
		
		return $this;
	}
	
	/**
	 * Gets the fk of the related type commune.
	 * 
	 * @return string
	 */
	public function getFkTypeCommune() : string
	{
		return $this->_fkTypeCommune;
	}
	
	/**
	 * Sets the fk of the related departement.
	 * 
	 * @param string $fkDepartement
	 * @return ApiFrInseeCogCommuneInterface
	 */
	public function setFkDepartement(string $fkDepartement) : ApiFrInseeCogCommuneInterface
	{
		$this->_fkDepartement = $fkDepartement;
		
		return $this;
	}
	
	/**
	 * Gets the fk of the related departement.
	 * 
	 * @return string
	 */
	public function getFkDepartement() : string
	{
		return $this->_fkDepartement;
	}
	
	/**
	 * Sets the fk of the related parent commune, if any.
	 * 
	 * @param ?string $fkCommuneParent
	 * @return ApiFrInseeCogCommuneInterface
	 */
	public function setFkCommuneParent(?string $fkCommuneParent) : ApiFrInseeCogCommuneInterface
	{
		$this->_fkCommuneParent = $fkCommuneParent;
		
		return $this;
	}
	
	/**
	 * Gets the fk of the related parent commune, if any.
	 * 
	 * @return ?string
	 */
	public function getFkCommuneParent() : ?string
	{
		return $this->_fkCommuneParent;
	}
	
	/**
	 * Sets the fk of the type of name of this commune.
	 * 
	 * @param int $fkTncc
	 * @return ApiFrInseeCogCommuneInterface
	 */
	public function setFkTncc(int $fkTncc) : ApiFrInseeCogCommuneInterface
	{
		$this->_fkTncc = $fkTncc;
		
		return $this;
	}
	
	/**
	 * Gets the fk of the type of name of this commune.
	 * 
	 * @return int
	 */
	public function getFkTncc() : int
	{
		return $this->_fkTncc;
	}
	
	/**
	 * Sets the name of this commune.
	 * 
	 * @param string $ncc
	 * @return ApiFrInseeCogCommuneInterface
	 */
	public function setNcc(string $ncc) : ApiFrInseeCogCommuneInterface
	{
		$this->_ncc = $ncc;
		
		return $this;
	}
	
	/**
	 * Gets the name of this commune.
	 * 
	 * @return string
	 */
	public function getNcc() : string
	{
		return $this->_ncc;
	}
	
	/**
	 * Sets the enriched name of this commune.
	 * 
	 * @param string $nccenr
	 * @return ApiFrInseeCogCommuneInterface
	 */
	public function setNccenr(string $nccenr) : ApiFrInseeCogCommuneInterface
	{
		$this->_nccenr = $nccenr;
		
		return $this;
	}
	
	/**
	 * Gets the enriched name of this commune.
	 * 
	 * @return string
	 */
	public function getNccenr() : string
	{
		return $this->_nccenr;
	}
	
}
