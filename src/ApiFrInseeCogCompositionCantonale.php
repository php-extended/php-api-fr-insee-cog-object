<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-cog-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCog;

/**
 * ApiFrInseeCogCompositionCantonale class file.
 * 
 * This is a simple implementation of the
 * ApiFrInseeCogCompositionCantonaleInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrInseeCogCompositionCantonale implements ApiFrInseeCogCompositionCantonaleInterface
{
	
	/**
	 * The id of this composition cantonale.
	 * 
	 * @var int
	 */
	protected int $_id;
	
	/**
	 * The libelle of this composition cantonale.
	 * 
	 * @var string
	 */
	protected string $_libelle;
	
	/**
	 * Constructor for ApiFrInseeCogCompositionCantonale with private members.
	 * 
	 * @param int $id
	 * @param string $libelle
	 */
	public function __construct(int $id, string $libelle)
	{
		$this->setId($id);
		$this->setLibelle($libelle);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of this composition cantonale.
	 * 
	 * @param int $id
	 * @return ApiFrInseeCogCompositionCantonaleInterface
	 */
	public function setId(int $id) : ApiFrInseeCogCompositionCantonaleInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the id of this composition cantonale.
	 * 
	 * @return int
	 */
	public function getId() : int
	{
		return $this->_id;
	}
	
	/**
	 * Sets the libelle of this composition cantonale.
	 * 
	 * @param string $libelle
	 * @return ApiFrInseeCogCompositionCantonaleInterface
	 */
	public function setLibelle(string $libelle) : ApiFrInseeCogCompositionCantonaleInterface
	{
		$this->_libelle = $libelle;
		
		return $this;
	}
	
	/**
	 * Gets the libelle of this composition cantonale.
	 * 
	 * @return string
	 */
	public function getLibelle() : string
	{
		return $this->_libelle;
	}
	
}
