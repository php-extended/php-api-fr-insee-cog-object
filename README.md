# php-extended/php-api-fr-insee-cog-object

A library that implements the php-api-fr-insee-cog-interface library.

This library makes insee Official Geographic Code (COG) available as objects
to populate a database. The COG is available at the url :
[https://www.insee.fr/fr/information/2560452](https://www.insee.fr/fr/information/2560452).

![coverage](https://gitlab.com/php-extended/php-api-fr-insee-cog-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-api-fr-insee-cog-object/badges/master/coverage.svg?style=flat-square)


## Last Updated Date : 2020-09-27


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-api-fr-insee-cog-object ^8`


## Basic Usage

This library gives an unique endpoint : `\PhpExtended\Insee\InseeCogEndpoint`
from which all data can be retrieved.

- For retrieving pays, you may use :

```php

$endpoint = new InseeCogEndpoint();
$paysIterator = $endpoint->getPaysIterator($endpoint->getMaximumAvailableYear());
foreach($paysIterator as $pays)
{
	// @var $pays \PhpExtended\Insee\InseeCogPaysInterface
}

```

If you want all the pays by year, do :

```php

$endpoint = new InseeCogEndpoint();
foreach($year = $endpoint->getMinimumAvailableYear(); $year <= $endpoint->getMaximumAvailableYear(); $year++)
{
	foreach($endpoint->getPaysIterator($year) as $pays)
	{
		// @var $pays \PhpExtended\Insee\InseeCogPaysInterface
	}
}

```


- For retrieving regions, you may use :

```php

$endpoint = new InseeCogEndpoint();
$regionIterator = $endpoint->getRegionIterator($endpoint->getMaximumAvailableYear());
foreach($regionIterator as $region)
{
	// @var $region \PhpExtended\Insee\InseeCogRegionInterface
}

```

If you want all the regions by year, do :

```php

$endpoint = new InseeCogEndpoint();
foreach($year = $endpoint->getMinimumAvailableYear(); $year <= $endpoint->getMaximumAvailableYear(); $year++)
{
	foreach($endpoint->getRegionIterator($year) as $region)
	{
		// @var $region \PhpExtended\Insee\InseeCogRegionInterface
	}
}

```


- For retrieving departements, you may use :

```php

$endpoint = new InseeCogEndpoint();
$departementIterator = $endpoint->getDepartementIterator($endpoint->getMaximumAvailableYear());
foreach($departementIterator as $departement)
{
	// @var $departement \PhpExtended\Insee\InseeCogDepartementInterface
}

```

If you want all the departements by year, do :

```php

$endpoint = new InseeCogEndpoint();
foreach($year = $endpoint->getMinimumAvailableYear(); $year <= $endpoint->getMaximumAvailableYear(); $year++)
{
	foreach($endpoint->getDepartementIterator($year) as $departement)
	{
		// @var $departement \PhpExtended\Insee\InseeCogDepartementInterface
	}
}

```


- For retrieving arrondissements, you may use :

```php

$endpoint = new InseeCogEndpoint();
$arrondissementIterator = $endpoint->getArrondissementIterator($endpoint->getMaximumAvailableYear());
foreach($arrondissementIterator as $arrondissement)
{
	// @var $arrondissement \PhpExtended\Insee\InseeCogArrondissementInterface
}

```

If you want all the arrondissements by year, do :

```php

$endpoint = new InseeCogEndpoint();
foreach($year = $endpoint->getMinimumAvailableYear(); $year <= $endpoint->getMaximumAvailableYear(); $year++)
{
	foreach($endpoint->getArrondissementIterator($year) as $arrondissement)
	{
		// @var $arrondissement \PhpExtended\Insee\InseeCogArrondissementInterface
	}
}

```


- For retrieving cantons, you may use :

```php

$endpoint = new InseeCogEndpoint();
$cantonIterator = $endpoint->getCantonIterator($endpoint->getMaximumAvailableYear());
foreach($cantonIterator as $canton)
{
	// @var $canton \PhpExtended\Insee\InseeCogCantonInterface
}

```

If you want all the cantons by year, do :

```php

$endpoint = new InseeCogEndpoint();
foreach($year = $endpoint->getMinimumAvailableYear(); $year <= $endpoint->getMaximumAvailableYear(); $year++)
{
	foreach($endpoint->getCantonIterator($year) as $canton)
	{
		// @var $canton \PhpExtended\Insee\InseeCogCantonInterface
	}
}

```


- For retrieving communes, you may use :

```php

$endpoint = new InseeCogEndpoint();
$communeIterator = $endpoint->getCommuneIterator($endpoint->getMaximumAvailableYear());
foreach($communeIterator as $commune)
{
	// @var $commune \PhpExtended\Insee\InseeCogCommuneInterface
}

```

If you want all the communes by year, do :

```php

$endpoint = new InseeCogEndpoint();
foreach($year = $endpoint->getMinimumAvailableYear(); $year <= $endpoint->getMaximumAvailableYear(); $year++)
{
	foreach($endpoint->getCommuneIterator($year) as $commune)
	{
		// @var $commune \PhpExtended\Insee\InseeCogCommuneInterface
	}
}

```


- For retrieving commune events, you may use :

```php

$endpoint = new InseeCogEndpoint();
foreach($endpoint->getEventCommuneIterator($year) as $event)
{
	// @var $event \PhpExtended\Insee\InseeCogEventCommuneInterface
}

```


## License

- The code is under MIT (See [license file](LICENSE)).

- The data is under Open License ([English](https://www.etalab.gouv.fr/wp-content/uploads/2018/11/open-licence.pdf) / [Français](https://www.etalab.gouv.fr/wp-content/uploads/2017/04/ETALAB-Licence-Ouverte-v2.0.pdf))
